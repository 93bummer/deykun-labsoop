﻿namespace FigureApp
{
    partial class FigureApp
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelOfListOfFigures = new System.Windows.Forms.Label();
            this.typesOfFigures = new System.Windows.Forms.ComboBox();
            this.labelOfRequestFigures = new System.Windows.Forms.Label();
            this.panelOfSquare = new System.Windows.Forms.Panel();
            this.cordY4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cordY2 = new System.Windows.Forms.TextBox();
            this.cordX4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cordX2 = new System.Windows.Forms.TextBox();
            this.cordY3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cordY1 = new System.Windows.Forms.TextBox();
            this.cordX3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cordX1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelOfRequestCordinatesOfSquare = new System.Windows.Forms.Label();
            this.panelOfCircle = new System.Windows.Forms.Panel();
            this.color = new System.Windows.Forms.ComboBox();
            this.labelOfRequestColor = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labalOfRequestRadius = new System.Windows.Forms.Label();
            this.cordY = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.radius = new System.Windows.Forms.TextBox();
            this.cordX = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.labelOfRequestCordinatesOfCircle = new System.Windows.Forms.Label();
            this.AddButton = new System.Windows.Forms.Button();
            this.EditButton = new System.Windows.Forms.Button();
            this.ReadFileButton = new System.Windows.Forms.Button();
            this.SortByS = new System.Windows.Forms.Button();
            this.SortSquaresByP = new System.Windows.Forms.Button();
            this.listOfFigures = new System.Windows.Forms.ListView();
            this.deleteButton = new System.Windows.Forms.Button();
            this.panelOfSquare.SuspendLayout();
            this.panelOfCircle.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelOfListOfFigures
            // 
            this.labelOfListOfFigures.AutoSize = true;
            this.labelOfListOfFigures.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelOfListOfFigures.Location = new System.Drawing.Point(151, 9);
            this.labelOfListOfFigures.Name = "labelOfListOfFigures";
            this.labelOfListOfFigures.Size = new System.Drawing.Size(443, 24);
            this.labelOfListOfFigures.TabIndex = 1;
            this.labelOfListOfFigures.Text = "Список геометрических фигур и их параметров";
            this.labelOfListOfFigures.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // typesOfFigures
            // 
            this.typesOfFigures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typesOfFigures.FormattingEnabled = true;
            this.typesOfFigures.Items.AddRange(new object[] {
            "Квадрат",
            "Окружность"});
            this.typesOfFigures.Location = new System.Drawing.Point(12, 310);
            this.typesOfFigures.Name = "typesOfFigures";
            this.typesOfFigures.Size = new System.Drawing.Size(121, 21);
            this.typesOfFigures.TabIndex = 2;
            this.typesOfFigures.SelectedIndexChanged += new System.EventHandler(this.typesOfFigures_SelectedIndexChanged);
            // 
            // labelOfRequestFigures
            // 
            this.labelOfRequestFigures.AutoSize = true;
            this.labelOfRequestFigures.Location = new System.Drawing.Point(12, 294);
            this.labelOfRequestFigures.Name = "labelOfRequestFigures";
            this.labelOfRequestFigures.Size = new System.Drawing.Size(98, 13);
            this.labelOfRequestFigures.TabIndex = 3;
            this.labelOfRequestFigures.Text = "Выберите фигуру:";
            // 
            // panelOfSquare
            // 
            this.panelOfSquare.Controls.Add(this.cordY4);
            this.panelOfSquare.Controls.Add(this.label8);
            this.panelOfSquare.Controls.Add(this.cordY2);
            this.panelOfSquare.Controls.Add(this.cordX4);
            this.panelOfSquare.Controls.Add(this.label4);
            this.panelOfSquare.Controls.Add(this.label7);
            this.panelOfSquare.Controls.Add(this.cordX2);
            this.panelOfSquare.Controls.Add(this.cordY3);
            this.panelOfSquare.Controls.Add(this.label3);
            this.panelOfSquare.Controls.Add(this.label6);
            this.panelOfSquare.Controls.Add(this.cordY1);
            this.panelOfSquare.Controls.Add(this.cordX3);
            this.panelOfSquare.Controls.Add(this.label2);
            this.panelOfSquare.Controls.Add(this.label5);
            this.panelOfSquare.Controls.Add(this.cordX1);
            this.panelOfSquare.Controls.Add(this.label1);
            this.panelOfSquare.Controls.Add(this.labelOfRequestCordinatesOfSquare);
            this.panelOfSquare.Location = new System.Drawing.Point(179, 290);
            this.panelOfSquare.Name = "panelOfSquare";
            this.panelOfSquare.Size = new System.Drawing.Size(348, 141);
            this.panelOfSquare.TabIndex = 4;
            this.panelOfSquare.Visible = false;
            // 
            // cordY4
            // 
            this.cordY4.Location = new System.Drawing.Point(133, 107);
            this.cordY4.Name = "cordY4";
            this.cordY4.Size = new System.Drawing.Size(33, 20);
            this.cordY4.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(113, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "y4:";
            // 
            // cordY2
            // 
            this.cordY2.Location = new System.Drawing.Point(133, 54);
            this.cordY2.Name = "cordY2";
            this.cordY2.Size = new System.Drawing.Size(33, 20);
            this.cordY2.TabIndex = 4;
            // 
            // cordX4
            // 
            this.cordX4.Location = new System.Drawing.Point(67, 107);
            this.cordX4.Name = "cordX4";
            this.cordX4.Size = new System.Drawing.Size(33, 20);
            this.cordX4.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(113, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "y2:";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(47, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "x4:";
            // 
            // cordX2
            // 
            this.cordX2.Location = new System.Drawing.Point(67, 54);
            this.cordX2.Name = "cordX2";
            this.cordX2.Size = new System.Drawing.Size(33, 20);
            this.cordX2.TabIndex = 3;
            // 
            // cordY3
            // 
            this.cordY3.Location = new System.Drawing.Point(133, 81);
            this.cordY3.Name = "cordY3";
            this.cordY3.Size = new System.Drawing.Size(33, 20);
            this.cordY3.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(47, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "x2:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(113, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "y3:";
            // 
            // cordY1
            // 
            this.cordY1.Location = new System.Drawing.Point(133, 28);
            this.cordY1.Name = "cordY1";
            this.cordY1.Size = new System.Drawing.Size(33, 20);
            this.cordY1.TabIndex = 2;
            // 
            // cordX3
            // 
            this.cordX3.Location = new System.Drawing.Point(67, 81);
            this.cordX3.Name = "cordX3";
            this.cordX3.Size = new System.Drawing.Size(33, 20);
            this.cordX3.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(113, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "y1:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(47, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "x3:";
            // 
            // cordX1
            // 
            this.cordX1.Location = new System.Drawing.Point(67, 28);
            this.cordX1.Name = "cordX1";
            this.cordX1.Size = new System.Drawing.Size(33, 20);
            this.cordX1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(47, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "x1:";
            // 
            // labelOfRequestCordinatesOfSquare
            // 
            this.labelOfRequestCordinatesOfSquare.AutoSize = true;
            this.labelOfRequestCordinatesOfSquare.Location = new System.Drawing.Point(4, 4);
            this.labelOfRequestCordinatesOfSquare.Name = "labelOfRequestCordinatesOfSquare";
            this.labelOfRequestCordinatesOfSquare.Size = new System.Drawing.Size(207, 13);
            this.labelOfRequestCordinatesOfSquare.TabIndex = 0;
            this.labelOfRequestCordinatesOfSquare.Text = "Введите координаты вершин квадрата:";
            // 
            // panelOfCircle
            // 
            this.panelOfCircle.Controls.Add(this.color);
            this.panelOfCircle.Controls.Add(this.labelOfRequestColor);
            this.panelOfCircle.Controls.Add(this.label11);
            this.panelOfCircle.Controls.Add(this.labalOfRequestRadius);
            this.panelOfCircle.Controls.Add(this.cordY);
            this.panelOfCircle.Controls.Add(this.label10);
            this.panelOfCircle.Controls.Add(this.radius);
            this.panelOfCircle.Controls.Add(this.cordX);
            this.panelOfCircle.Controls.Add(this.label9);
            this.panelOfCircle.Controls.Add(this.labelOfRequestCordinatesOfCircle);
            this.panelOfCircle.Location = new System.Drawing.Point(179, 290);
            this.panelOfCircle.Name = "panelOfCircle";
            this.panelOfCircle.Size = new System.Drawing.Size(348, 141);
            this.panelOfCircle.TabIndex = 5;
            this.panelOfCircle.Visible = false;
            // 
            // color
            // 
            this.color.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.color.FormattingEnabled = true;
            this.color.Items.AddRange(new object[] {
            "Красный",
            "Жёлтый",
            "Зелёный"});
            this.color.Location = new System.Drawing.Point(176, 80);
            this.color.Name = "color";
            this.color.Size = new System.Drawing.Size(121, 21);
            this.color.TabIndex = 4;
            // 
            // labelOfRequestColor
            // 
            this.labelOfRequestColor.AutoSize = true;
            this.labelOfRequestColor.Location = new System.Drawing.Point(173, 59);
            this.labelOfRequestColor.Name = "labelOfRequestColor";
            this.labelOfRequestColor.Size = new System.Drawing.Size(149, 13);
            this.labelOfRequestColor.TabIndex = 5;
            this.labelOfRequestColor.Text = "Выберите цвет окружности:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(55, 83);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "R:";
            // 
            // labalOfRequestRadius
            // 
            this.labalOfRequestRadius.AutoSize = true;
            this.labalOfRequestRadius.Location = new System.Drawing.Point(7, 60);
            this.labalOfRequestRadius.Name = "labalOfRequestRadius";
            this.labalOfRequestRadius.Size = new System.Drawing.Size(153, 13);
            this.labalOfRequestRadius.TabIndex = 3;
            this.labalOfRequestRadius.Text = "Введите радиус окружности:";
            // 
            // cordY
            // 
            this.cordY.Location = new System.Drawing.Point(133, 27);
            this.cordY.Name = "cordY";
            this.cordY.Size = new System.Drawing.Size(30, 20);
            this.cordY.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(116, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "y:";
            // 
            // radius
            // 
            this.radius.Location = new System.Drawing.Point(76, 80);
            this.radius.Name = "radius";
            this.radius.Size = new System.Drawing.Size(30, 20);
            this.radius.TabIndex = 3;
            // 
            // cordX
            // 
            this.cordX.Location = new System.Drawing.Point(76, 27);
            this.cordX.Name = "cordX";
            this.cordX.Size = new System.Drawing.Size(30, 20);
            this.cordX.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(59, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "x:";
            // 
            // labelOfRequestCordinatesOfCircle
            // 
            this.labelOfRequestCordinatesOfCircle.AutoSize = true;
            this.labelOfRequestCordinatesOfCircle.Location = new System.Drawing.Point(7, 4);
            this.labelOfRequestCordinatesOfCircle.Name = "labelOfRequestCordinatesOfCircle";
            this.labelOfRequestCordinatesOfCircle.Size = new System.Drawing.Size(217, 13);
            this.labelOfRequestCordinatesOfCircle.TabIndex = 0;
            this.labelOfRequestCordinatesOfCircle.Text = "Введите координаты центра окружности:";
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(552, 281);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(174, 31);
            this.AddButton.TabIndex = 5;
            this.AddButton.Text = "Добавить";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // EditButton
            // 
            this.EditButton.Location = new System.Drawing.Point(552, 312);
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(174, 31);
            this.EditButton.TabIndex = 5;
            this.EditButton.Text = "Редактировать";
            this.EditButton.UseVisualStyleBackColor = true;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // ReadFileButton
            // 
            this.ReadFileButton.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ReadFileButton.Location = new System.Drawing.Point(552, 376);
            this.ReadFileButton.Name = "ReadFileButton";
            this.ReadFileButton.Size = new System.Drawing.Size(174, 31);
            this.ReadFileButton.TabIndex = 5;
            this.ReadFileButton.Text = "Считать информацию из файла";
            this.ReadFileButton.UseVisualStyleBackColor = true;
            this.ReadFileButton.Click += new System.EventHandler(this.ReadFileButton_Click);
            // 
            // SortByS
            // 
            this.SortByS.Location = new System.Drawing.Point(552, 407);
            this.SortByS.Name = "SortByS";
            this.SortByS.Size = new System.Drawing.Size(174, 31);
            this.SortByS.TabIndex = 5;
            this.SortByS.Text = "Сортировать по площади";
            this.SortByS.UseVisualStyleBackColor = true;
            this.SortByS.Click += new System.EventHandler(this.SortByS_Click);
            // 
            // SortSquaresByP
            // 
            this.SortSquaresByP.Location = new System.Drawing.Point(552, 439);
            this.SortSquaresByP.Name = "SortSquaresByP";
            this.SortSquaresByP.Size = new System.Drawing.Size(174, 31);
            this.SortSquaresByP.TabIndex = 5;
            this.SortSquaresByP.Text = "Сортировать по периметру";
            this.SortSquaresByP.UseVisualStyleBackColor = true;
            this.SortSquaresByP.Click += new System.EventHandler(this.SortSquaresByP_Click);
            // 
            // listOfFigures
            // 
            this.listOfFigures.HideSelection = false;
            this.listOfFigures.Location = new System.Drawing.Point(12, 45);
            this.listOfFigures.Name = "listOfFigures";
            this.listOfFigures.Size = new System.Drawing.Size(725, 220);
            this.listOfFigures.TabIndex = 6;
            this.listOfFigures.UseCompatibleStateImageBehavior = false;
            this.listOfFigures.View = System.Windows.Forms.View.List;
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(552, 344);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(174, 31);
            this.deleteButton.TabIndex = 5;
            this.deleteButton.Text = "Удалить";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // FigureApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 502);
            this.Controls.Add(this.listOfFigures);
            this.Controls.Add(this.panelOfCircle);
            this.Controls.Add(this.SortSquaresByP);
            this.Controls.Add(this.SortByS);
            this.Controls.Add(this.ReadFileButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.EditButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.panelOfSquare);
            this.Controls.Add(this.labelOfRequestFigures);
            this.Controls.Add(this.typesOfFigures);
            this.Controls.Add(this.labelOfListOfFigures);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FigureApp";
            this.Text = "FigureApp";
            this.panelOfSquare.ResumeLayout(false);
            this.panelOfSquare.PerformLayout();
            this.panelOfCircle.ResumeLayout(false);
            this.panelOfCircle.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelOfListOfFigures;
        private System.Windows.Forms.ComboBox typesOfFigures;
        private System.Windows.Forms.Label labelOfRequestFigures;
        private System.Windows.Forms.Panel panelOfSquare;
        private System.Windows.Forms.Label labelOfRequestCordinatesOfSquare;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox cordY4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox cordY2;
        private System.Windows.Forms.TextBox cordX4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox cordX2;
        private System.Windows.Forms.TextBox cordY3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox cordY1;
        private System.Windows.Forms.TextBox cordX3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox cordX1;
        private System.Windows.Forms.Panel panelOfCircle;
        private System.Windows.Forms.Label labelOfRequestCordinatesOfCircle;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox cordY;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox cordX;
        private System.Windows.Forms.Label labalOfRequestRadius;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox radius;
        private System.Windows.Forms.Label labelOfRequestColor;
        private System.Windows.Forms.ComboBox color;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button EditButton;
        private System.Windows.Forms.Button ReadFileButton;
        private System.Windows.Forms.Button SortByS;
        private System.Windows.Forms.Button SortSquaresByP;
        private System.Windows.Forms.ListView listOfFigures;
        private System.Windows.Forms.Button deleteButton;
    }
}

