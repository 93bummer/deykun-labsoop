﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using FiguresLibrary;

namespace FigureApp
{
    public partial class FigureApp : Form
    {
        private CatalogOfFigures catalogOfFigures;
        public FigureApp()
        {
            InitializeComponent();

            catalogOfFigures = new CatalogOfFigures();
        }

        private System.Drawing.Color GetChoosenColor(int index)
        {
            switch (index)
            {
                case 0:
                    return System.Drawing.Color.Red;
                case 1:
                    return System.Drawing.Color.Yellow;
                case 2:
                    return System.Drawing.Color.Green;
                default:
                    return System.Drawing.Color.Black;
            }
        }    

        private void AddEventToLog(string info, System.Drawing.Color color)
        {
            ListViewItem ItemViewer = new ListViewItem();

            ItemViewer.Text = info;
            ItemViewer.ForeColor = color;

            listOfFigures.Items.Add(ItemViewer);
        }

        private void typesOfFigures_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (typesOfFigures.SelectedIndex)
            {
                case 0:
                    panelOfSquare.Visible = true;
                    panelOfCircle.Visible = false; 
                    break;
                case 1:
                    panelOfSquare.Visible = true;
                    panelOfCircle.Visible = true;
                    break;
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            switch (typesOfFigures.SelectedIndex)
            {
                case 0:
                    catalogOfFigures.AddFigure(new int[2] { typesOfFigures.SelectedIndex, catalogOfFigures.Count }, Convert.ToDouble(cordX1.Text), Convert.ToDouble(cordY1.Text), Convert.ToDouble(cordX2.Text), Convert.ToDouble(cordY2.Text), Convert.ToDouble(cordX3.Text), Convert.ToDouble(cordY3.Text), Convert.ToDouble(cordX4.Text), Convert.ToDouble(cordY4.Text));
                    AddEventToLog(catalogOfFigures.GetFigureInfo(catalogOfFigures.Count - 1), System.Drawing.Color.Black);
                    break;
                case 1:
                    catalogOfFigures.AddFigure(new int[3] { typesOfFigures.SelectedIndex, catalogOfFigures.Count, color.SelectedIndex }, Convert.ToDouble(cordX.Text), Convert.ToDouble(cordY.Text), Convert.ToDouble(radius.Text));
                    AddEventToLog(catalogOfFigures.GetFigureInfo(catalogOfFigures.Count - 1), GetChoosenColor(color.SelectedIndex));
                    break;
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            Regex square = new Regex("Квадрат");

            if(square.IsMatch(listOfFigures.SelectedItems[0].Text))
                catalogOfFigures.AddFigure(new int[2] { 0, listOfFigures.SelectedIndices[0] }, Convert.ToDouble(cordX1.Text), Convert.ToDouble(cordY1.Text), Convert.ToDouble(cordX2.Text), Convert.ToDouble(cordY2.Text), Convert.ToDouble(cordX3.Text), Convert.ToDouble(cordY3.Text), Convert.ToDouble(cordX4.Text), Convert.ToDouble(cordY4.Text));
            else
            {
                catalogOfFigures.AddFigure(new int[3] { 1, listOfFigures.SelectedIndices[0], color.SelectedIndex }, Convert.ToDouble(cordX.Text), Convert.ToDouble(cordY.Text), Convert.ToDouble(radius.Text));
                listOfFigures.Items[listOfFigures.SelectedIndices[0]].ForeColor = GetChoosenColor(color.SelectedIndex);
            }  

            catalogOfFigures.Count--;

            listOfFigures.Items[listOfFigures.SelectedIndices[0]].Text = catalogOfFigures.GetFigureInfo(listOfFigures.SelectedIndices[0]);
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            catalogOfFigures.DeleteFigure(listOfFigures.SelectedIndices[0]);

            listOfFigures.Items.Remove(listOfFigures.SelectedItems[0]);
        }

        private void ReadFileButton_Click(object sender, EventArgs e)
        {
            int oldIndex = catalogOfFigures.Count,
                counter = 0;
            int[] colors = catalogOfFigures.ReadInfoFromFile();

            for (int i = oldIndex; i < catalogOfFigures.Count; i++, counter++)
                AddEventToLog(catalogOfFigures.GetFigureInfo(i), GetChoosenColor(colors[counter]));                       
        }

        private void SortByS_Click(object sender, EventArgs e)
        {
            catalogOfFigures.SortByS();

            listOfFigures.Items.Clear();

            for(int i = 0; i < catalogOfFigures.Count; i++)
                AddEventToLog(catalogOfFigures.GetFigureInfo(i), GetChoosenColor(catalogOfFigures.GetColor(i)));
        }

        private void SortSquaresByP_Click(object sender, EventArgs e)
        {
            catalogOfFigures.SortSquaresByP();

            listOfFigures.Items.Clear();

            for (int i = 0; i < catalogOfFigures.Count; i++)
                AddEventToLog(catalogOfFigures.GetFigureInfo(i), GetChoosenColor(catalogOfFigures.GetColor(i)));
        }
    }
}
