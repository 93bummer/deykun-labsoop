﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FiguresLibrary.Tests
{
    [TestClass]
    public class FiguresLibraryTests
    {
        private Random random = new Random();
        private IFigure figure;
        private double eps = 0.00001;

        [TestMethod]
        public void STets()
        {
            double answer;
            double radius;

            for (int i = 0; i < 10; i++)
            {
                radius = Convert.ToDouble(random.Next(0, 100));
                figure = new Circle(1, 1, radius, 0);

                answer = Math.PI * Math.Pow(radius, 2);

                //Console.WriteLine($"Массив ответов - {answer}; Массив свойств - {figure.S}");

                if (Math.Abs(figure.S - answer) < eps)
                    Assert.IsTrue(true);
                else
                    Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void CalcPTest()
        {
            double[,] cords = new double[,] { { 1, 2, 2, 2, 1, 2, 1, 1 }, { 1, 4, 3, 4, 3, 2, 1, 2 }, { -2, 2, 0, 2, 0, 0, -2, 0 } };
            double answer;

            for (int i = 0; i < 3; i++)
            {
                figure = new Square(cords[i, 0], cords[i, 1], cords[i, 2], cords[i, 3], cords[i, 4], cords[i, 5], cords[i, 6], cords[i, 7]);

                answer = 4 * Math.Sqrt(Math.Pow(cords[i, 2] - cords[i, 0], 2) - Math.Pow(cords[i, 3] - cords[i, 1], 2));

                Console.WriteLine($"Массив ответов - {answer}; Массив периметров - {((Square)figure).CalcP()}");

                if (Math.Abs(((Square)figure).CalcP() - answer) < eps)
                    Assert.IsTrue(true);
                else
                    Assert.IsTrue(false);
            }
        }
    }
}
