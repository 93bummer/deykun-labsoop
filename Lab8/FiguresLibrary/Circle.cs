﻿using System;

namespace FiguresLibrary
{
    /// <summary>
    /// Цвет фигуры
    /// </summary>
    public enum Color
    {
        Red,
        Yellow,
        Green
    }

    public class Circle : IFigure
    {
        /// <summary>
        /// Поле начальной координаты x
        /// </summary>
        private double x;
        /// <summary>
        /// Поле начальной координаты y
        /// </summary>
        private double y;
        /// <summary>
        /// Радиус окружности
        /// </summary>
        private double r;
        /// <summary>
        /// Цвет окружности
        /// </summary>
        private Color color;

        public Color Color
        {
            get
            {
                return color;
            }
        }

        /// <summary>
        /// Свойство возвращающее площадь фигуры
        /// </summary>
        public double S
        {
            get
            {
                return Math.PI * r * r;
            }
        }

        /// <summary>
        /// Индексатор, для доступа к параметрам фигуры
        /// </summary>
        /// <param name="nameOfParam">Индекс искомого параметра</param>
        /// <returns>Значение параметра</returns>
        public double this[string nameOfParam]
        {
            get
            {
                switch (nameOfParam)
                {
                    case "x":
                        return x;
                    case "y":
                        return y;
                    case "r":
                        return r;
                    case "color":
                        return (double)color;
                }
                return 0;
            }
            set
            {
                switch (nameOfParam)
                {
                    case "x":
                        x = value;
                        break;
                    case "y":
                        y = value;
                        break;
                    case "r":
                        r = value;
                        break;
                    case "color":
                        color = (Color)value;
                        break;
                }
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="r">Радиус окружности</param>
        public Circle(double x, double y, double r, int color)
        {
            this.x = x;
            this.y = y;
            this.r = r;
            this.color = (Color)color;
        }

        /// <summary>
        /// Метод вывода информации
        /// </summary>
        public string GetInfo()
        {
            return $"Окружность, координаты: x = {x}, y = {y}; Радиус = {r}, Цвет - {color}; Площадь - {S}";
        }
    }
}
