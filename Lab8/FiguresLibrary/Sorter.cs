﻿using System.Collections.Generic;

namespace FiguresLibrary
{
    /// <summary>
    /// Класс, который отвечает за сравнение двух объектов
    /// </summary>
    class Sorter : IComparer<IFigure>
    {
        /// <summary>
        /// Метод, который сравнивает площади двух фигур
        /// </summary>
        /// <param name="firstFigure">Первая фигура</param>
        /// <param name="secondFigure">Вторая фигура</param>
        /// <returns>0 - первая фигура меньше, 1 - вторая фигура меньше</returns>
        public int Compare(IFigure firstFigure, IFigure secondFigure)
        {
            if (firstFigure.S < secondFigure.S)
                return 0;
            else
                return 1;
        }

        /// <summary>
        /// Метод, который сравнивает периметр двух квадратов
        /// </summary>
        /// <param name="firstSquare">Первый квадрат</param>
        /// <param name="secondSquare">Второй квадрат</param>
        /// <returns>0 - первый квадрат больше, 1 - второй квадрат больше</returns>
        public int SortRedSquaresByP(Square firstSquare, Square secondSquare)
        {
            if (firstSquare.CalcP() > secondSquare.CalcP())
                return 0;
            else
                return 1;
        }
    }
}
