﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace FiguresLibrary
{
    /// <summary>
    /// Класс, который производит учёт фигур
    /// </summary>
    public class CatalogOfFigures
    {
        /// <summary>
        /// Массив фигур
        /// </summary>
        private IFigure[] figures;
        /// <summary>
        /// Количество фигур
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public CatalogOfFigures()
        {
            figures = new IFigure[15];
            Count = 0;
        }

        /// <summary>
        /// Метод, который позволяет добавить фигуру
        /// </summary>
        /// <param name="indexes">Массив содержащий тип фигуры, её порядковый номер и цвет</param>
        /// <param name="cords">Параметры фигуры</param>
        public void AddFigure(int[] indexes, params double[] cords)
        {
            switch (indexes[0])
            {
                case 0:
                    figures[indexes[1]] = new Square(cords);
                    break;
                case 1:
                    figures[indexes[1]] = new Circle(cords[0], cords[1], cords[2], indexes[2]);
                    break;
            }
            Count++;
        }

        /// <summary>
        /// Метод, который позволяет удалить фигуру
        /// </summary>
        /// <param name="index">Индекс удаляемой фигуры</param>
        public void DeleteFigure(int index)
        {
            for (int i = index; i < Count; i++)
                figures[i] = figures[i + 1];

            Count--;
        }

        /// <summary>
        /// Метод, который предоставляет информацию о фигуре
        /// </summary>
        /// <param name="index">Индекс фигуры</param>
        /// <returns>Информация о фигуре в виде строки</returns>
        public string GetFigureInfo(int index)
        {
            return figures[index].GetInfo();
        }

        /// <summary>
        /// Метод, который позволяет добавить фигуры, информация о которых записана в текстовом файле
        /// </summary>
        /// <returns>Цвета фигур</returns>
        public int[] ReadInfoFromFile()
        {
            string[] figuresInfo = File.ReadAllLines("Data.txt");
            string[] values;
            int[] colors = new int[15];
            int counter = 0;
            Regex pattern = new Regex(@"\s");

            foreach (string figureInfo in figuresInfo)
            {
                values = pattern.Split(figureInfo);

                if (new Regex("Квадрат").IsMatch(figureInfo))
                {
                    AddFigure(new int[2] { 0, Count }, Convert.ToDouble(values[1]), Convert.ToDouble(values[2]), Convert.ToDouble(values[3]), Convert.ToDouble(values[4]), Convert.ToDouble(values[5]), Convert.ToDouble(values[6]), Convert.ToDouble(values[7]), Convert.ToDouble(values[8]));
                    colors[counter] = 3;
                }    
                else
                {
                    AddFigure(new int[3] { 1, Count, Convert.ToInt32(values[4]) }, Convert.ToDouble(values[1]), Convert.ToDouble(values[2]), Convert.ToDouble(values[3]));
                    colors[counter] = Convert.ToInt32(values[4]);
                }

                counter++;
            }

            return colors;
        }

        /// <summary>
        /// Метод, который предоставляет доступ к цвету фигуры
        /// </summary>
        /// <param name="index">Индекс фигуры</param>
        /// <returns>Цвет фигуры</returns>
        public int GetColor(int index)
        {
            if (figures[index] is Circle)
                return (int)((Circle)figures[index]).Color;
            else
                return 3;
        }

        /// <summary>
        /// Метод, который сортирует фигуры по площади
        /// </summary>
        public void SortByS()
        {
            for (int i = 0; i < Count - 1; i++)
                for (int j = 0; j < Count - i - 1; j++)
                    if (new Sorter().Compare(figures[j + 1], figures[j]) == 0)
                        (figures[j + 1], figures[j]) = (figures[j], figures[j + 1]);  
        }

        /// <summary>
        /// Метод, который сортирует квадраты по периметру по убыванию
        /// </summary>
        public void SortSquaresByP()
        {
            int countSq = 0, countCir = 0;
            Square[] squares = new Square[15];
            Circle[] circles = new Circle[15];

            for (int i = 0; i < Count; i++)
                if (figures[i] is Square)
                {
                    squares[countSq] = (Square)figures[i];
                    countSq++;
                }
                else
                {
                    circles[countCir] = (Circle)figures[i];
                    countCir++;
                }

            for (int i = 0; i < countSq - 1; i++)
                for (int j = 0; j < countSq - i - 1; j++)
                    if (new Sorter().SortRedSquaresByP(squares[j + 1], squares[j]) == 0)
                        (squares[j + 1], squares[j]) = (squares[j], squares[j + 1]);

            for (int i = 0; i < countSq; i++)
                figures[i] = squares[i];
            for (int i = countSq, j = 0; i < Count; i++, j++)
                figures[i] = circles[j];
        }
    }
}
