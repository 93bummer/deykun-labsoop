﻿using System;

namespace FiguresLibrary
{
    public class Square : IFigure
    {
        /// <summary>
        /// Поле-массив, который содержит координаты вершин
        /// </summary>
        private double[,] cords = new double[4, 2];

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="cords">Координаты</param>
        public Square(params double[] cords)
        {
            int count = 0;

            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 2; j++)
                {
                    this.cords[i, j] = cords[count];
                    count++;
                } 
        }

        /// <summary>
        /// Свойство возвращающее площадь фигуры
        /// </summary>
        public double S
        {
            get
            {
                return Math.Pow(cords[1, 0] - cords[0, 0], 2) + Math.Pow(cords[1, 1] - cords[0, 1], 2);
            }
        }

        /// <summary>
        /// Индексатор, для доступа к параметрам фигуры
        /// </summary>
        /// <param name="nameOfParam">Индекс искомого параметра</param>
        /// <returns>Значение параметра</returns>
        public double this[string nameOfParam]
        {
            get
            {
                switch (nameOfParam)
                {
                    case "x1":
                        return cords[0, 0];
                    case "y1":
                        return cords[0, 1];
                    case "x2":
                        return cords[1, 0];
                    case "y2":
                        return cords[1, 1];
                    case "x3":
                        return cords[2, 0];
                    case "y3":
                        return cords[2, 1];
                    case "x4":
                        return cords[3, 0];
                    case "y4":
                        return cords[3, 1];
                }
                return 0;
            }
            set
            {
                switch (nameOfParam)
                {
                    case "x1":
                        cords[0, 0] = value;
                        break;
                    case "y1":
                        cords[0, 1] = value;
                        break;
                    case "x2":
                        cords[1, 0] = value;
                        break;
                    case "y2":
                        cords[1, 1] = value;
                        break;
                    case "x3":
                        cords[2, 0] = value;
                        break;
                    case "y3":
                        cords[2, 1] = value;
                        break;
                    case "x4":
                        cords[3, 0] = value;
                        break;
                    case "y4":
                        cords[3, 1] = value;
                        break;
                }
            }
        }

        /// <summary>
        /// Метод, который считает периметр
        /// </summary>
        /// <returns>Периметр</returns>
        public double CalcP()
        {
            return Math.Sqrt(Math.Pow(cords[1, 0] - cords[0, 0], 2) + Math.Pow(cords[1, 1] - cords[0, 1], 2)) * 4;
        }

        /// <summary>
        /// Метод, который предоставляет информацию о фигуре
        /// </summary>
        /// <returns>Строка с параметрами фигуры</returns>
        public string GetInfo()
        {
            return $"Квадрат, координаты: A({cords[0, 0]}, {cords[0, 1]}), B({cords[1, 0]}, {cords[1, 1]})," +
                $" C({cords[2, 0]}, {cords[2, 1]}), D({cords[3, 0]}, {cords[3, 1]});" +
                $" Периметр - {this.CalcP().ToString()}; Площадь - {S}";
        }
    }
}
