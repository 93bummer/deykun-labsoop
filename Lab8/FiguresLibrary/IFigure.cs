﻿namespace FiguresLibrary
{
    public interface IFigure
    {
        /// <summary>
        /// Свойство возвращающее площадь фигуры
        /// </summary>
        double S { get; }

        /// <summary>
        /// Индексатор, для доступа к параметрам фигуры
        /// </summary>
        /// <param name="nameOfParam">Индекс искомого параметра</param>
        /// <returns>Значение параметра</returns>
        double this[string nameOfParam] { get; set; }

        /// <summary>
        /// Метод вывода информации
        /// </summary>
        string GetInfo();
    }
}
