﻿using System;
using System.Windows;
using PolynomialLibrary;
using ExceptionLibrary;
using System.Text.RegularExpressions;

namespace PolinomialCalculator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Polynomial[] polynomials;
        private int multiplier;

        public MainWindow()
        {
            InitializeComponent();

            polynomials = new Polynomial[2];
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            firstPolyBox.Text = "";
            secondPolyBox.Text = "";
            multiplierBox.Text = "";
        }

        private void polynomialMultiplication_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (firstPolyBox.Text == "" || secondPolyBox.Text == "")
                    throw new PolynomialFieldIsEmpty();
                else
                {
                    if (StringParser())
                        answerField.Text = $"{polynomials[0]} * {polynomials[1]} = {polynomials[0] * polynomials[1]}";
                }
            }
            catch (PolynomialFieldIsEmpty)
            {
                MessageBox.Show("Поля для ввода полиномов должны быть заполнены.");
            }
        }

        private void multiplyPolynomial_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (firstPolyBox.Text == "" || secondPolyBox.Text == "")
                    throw new PolynomialFieldIsEmpty();

                if (multiplierBox.Text == "")
                    throw new MultiplierFieldIsEmpty();
                else
                {
                    if (!Int32.TryParse(multiplierBox.Text, out multiplier))
                        throw new IncorrectlyInputtedMultiplier();
                    else
                    {
                        if(StringParser())
                            answerField.Text = $"{multiplier} * {polynomials[0]} = {multiplier * polynomials[0]}  |  {multiplier} * {polynomials[1]} = {multiplier * polynomials[1]}";
                    }
                }
            }
            catch (PolynomialFieldIsEmpty)
            {
                MessageBox.Show("Поля для ввода полиномов должны быть заполнены.");
            }
            catch (MultiplierFieldIsEmpty)
            {
                MessageBox.Show("Полe для ввода множителя должно быть заполнено.");
            }
            catch (IncorrectlyInputtedMultiplier)
            {
                MessageBox.Show("Значением поля множителя должно быть целое число.");
            }
        }

        private bool StringParser()
        {
            int maxCoeff;
            double[] odds;
            string[] powers, oddsArr;
            string[] polyTextValue = new string[2] { firstPolyBox.Text, secondPolyBox.Text };
            Regex powerPattern = new Regex(@"\+?\-?\d+x\^");
            Regex oddsPattern = new Regex(@"x\^\d+");

            try
            {
                if (!powerPattern.IsMatch(polyTextValue[0]) || !powerPattern.IsMatch(polyTextValue[1]) || !oddsPattern.IsMatch(polyTextValue[0]) || !oddsPattern.IsMatch(polyTextValue[1]))
                    throw new InvalidPolynomialInputFormat();
                else
                {
                    for (int k = 0; k < 2; k++)
                    {
                        maxCoeff = 0;

                        powers = powerPattern.Split(polyTextValue[k]);
                        maxCoeff = Convert.ToInt32(powers[1]);
                        odds = new double[maxCoeff + 1];

                        oddsArr = oddsPattern.Split(polyTextValue[k]);

                        for (int i = maxCoeff, j = 1, count = 0; i >= 0; i--)
                            if (i == Convert.ToInt32(powers[j]))
                            {
                                odds[i] = Convert.ToInt32(oddsArr[count]);
                                count++;
                                j++;
                            }
                            else
                                odds[i] = 0;

                        polynomials[k] = new Polynomial(maxCoeff, odds);
                    }

                    return true;
                }
            }
            catch
            {
                MessageBox.Show("Неверный формат введённого вами полинома.\n(Пример: 5x^8+10x^1-10x^0)");
                return false;
            } 
        }
    }
}
