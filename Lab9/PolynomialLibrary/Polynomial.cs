﻿using System;

namespace PolynomialLibrary
{
    /// <summary>
    /// Класс описывающий полином
    /// </summary>
    public class Polynomial
    {
        /// <summary>
        /// Свойство хранящее степень полинома.
        /// </summary>
        private int power;
        /// <summary>
        /// Свойство хранящее коэффициенты полинома в массиве.
        /// </summary>
        private double[] odds;

        /// <summary>
        /// Конструктор. Создаёт полином.
        /// </summary>
        /// <param name="power">Степень полинома.</param>
        /// <param name="odds">Коэффициенты при аргументах.</param>
        public Polynomial(int power, double[] odds)
        {
            this.power = power;
            this.odds = odds;
        }

        /// <summary>
        /// Перегрегрузка оператора '*'. Метод-обёртка.
        /// </summary>
        /// <param name="firstPoly">Первый полином.</param>
        /// <param name="secondPoly">Второй полином.</param>
        /// <returns>Полином полученный после умножения полиномов.</returns>
        public static Polynomial operator *(Polynomial firstPoly, Polynomial secondPoly)
        {
            return PolynomialMultiplication(firstPoly, secondPoly);
        }
        /// <summary>
        /// Перегрегрузка оператора '*'. Метод-обёртка.
        /// </summary>
        /// <param name="multiplier">Множитель.</param>
        /// <param name="polynomial">Полином.</param>
        /// <returns>Полином.</returns>
        public static Polynomial operator *(int multiplier, Polynomial polynomial)
        {
            return MultiplyPolynomial(multiplier, polynomial);
        }
        /// <summary>
        /// Перегрегрузка оператора '*'. Метод умножает два полинома.
        /// </summary>
        /// <param name="firstPoly">Первый полином.</param>
        /// <param name="secondPoly">Второй полином.</param>
        /// <returns>Полином полученный после умножения полиномов.</returns>
        private static Polynomial PolynomialMultiplication(Polynomial firstPoly, Polynomial secondPoly)
        {
            double[] newOdds = new double[firstPoly.power + secondPoly.power + 1];

            for (int i = 0; i <= firstPoly.power; i++)
                for (int j = 0; j <= secondPoly.power; j++)
                    newOdds[i + j] += firstPoly.odds[i] * secondPoly.odds[j];

            return new Polynomial(firstPoly.power + secondPoly.power, newOdds);
        }
        /// <summary>
        /// Перегрегрузка оператора '*'. Метод умножает полином на число.
        /// </summary>
        /// <param name="multiplier">Множитель.</param>
        /// <param name="polynomial">Полином.</param>
        /// <returns>Полином.</returns>
        private static Polynomial MultiplyPolynomial(int multiplier, Polynomial polynomial)
        {
            double[] newOdds = new double[polynomial.power + 1];

            for (int i = 0; i <= polynomial.power; i++)
                newOdds[i] = polynomial.odds[i] * multiplier;

            return new Polynomial(polynomial.power, newOdds);
        }

        /* Вывод полинома в строку */

        /// <summary>
        /// Переопределённый метод, который выводит полином в естественном виде.
        /// </summary>
        /// <returns>Полином в виде строки.</returns>
        public override string ToString()
        {
            int i, decade = 0, one = 0;
            string value = null;
            bool isFirst = true;

            for (i = power; i >= 0 && i <= power && isFirst; i--)
            {
                if (i > 9)
                {
                    decade = i / 10;
                    one = i % 10;
                }

                switch (odds[i])
                {
                    case 0:
                        break;
                    case 1:
                        isFirst = false;
                        break;
                    case -1:
                        value += "-";
                        isFirst = false;
                        break;
                    default:
                        value += odds[i].ToString();
                        isFirst = false;
                        break;
                }

                switch (i)
                {
                    case 0:
                        if (odds[i] == 1 || odds[i] == -1)
                            value += "1";
                        break;
                    case 1:
                        if (odds[i] != 0)
                        {
                            value += "x";
                        }
                        break;
                    default:
                        if (odds[i] != 0)
                        {
                            if (i > 9)
                            {
                                value += "x" + DigitToSuperscript(decade) + DigitToSuperscript(one);
                            }
                            else
                                value += "x" + DigitToSuperscript(i);
                        }
                        break;
                }
            }

            for (; i >= 0 && i <= power && !isFirst; i--)
            {
                if (i > 9)
                {
                    decade = i / 10;
                    one = i % 10;
                }

                switch (odds[i])
                {
                    case 0:
                        break;
                    case 1:
                        value += ConvertToSymbol(odds[i]);
                        break;
                    case -1:
                        value += ConvertToSymbol(odds[i]);
                        break;
                    default:
                        value += ConvertToSymbol(odds[i]) + Math.Abs(odds[i]).ToString();
                        break;
                }

                switch (i)
                {
                    case 0:
                        if (odds[i] == 1 || odds[i] == -1)
                            value += "1";
                        break;
                    case 1:
                        if (odds[i] != 0)
                        {
                            value += "x";
                        }
                        break;
                    default:
                        if (odds[i] != 0)
                        {
                            if (i > 9)
                            {
                                value += "x" + DigitToSuperscript(decade) + DigitToSuperscript(one);
                            }
                            else
                                value += "x" + DigitToSuperscript(i);
                        }
                        break;
                }
            }

            if (value == null)
                value = " 0";

            return value;
        }
        /// <summary>
        /// Метод определяющий знак числа.
        /// </summary>
        /// <param name="value">Число, знак которого нужно определить.</param>
        /// <returns>Знак в виде символа.</returns>
        private string ConvertToSymbol(double value)
        {
            if (value < 0)
                return "-";
            else
                return "+";
        }
        /// <summary>
        /// Метод преобразующий число в код символа.
        /// </summary>
        /// <param name="digit">Число, которое преобразуется.</param>
        /// <returns>Символ юникода.</returns>
        private char DigitToSuperscript(int digit)
        {
            switch (digit)
            {
                case 1: return '\u00b9';
                case 2: return '\u00b2';
                case 3: return '\u00b3';
                default: return (char)(0x2070 + digit);
            }
        }
    }
}
