﻿using System;

namespace ExceptionLibrary
{
    /// <summary>
    /// Класс описывающий исключение для некорректно заполненного поля множителя
    /// </summary>
    public class IncorrectlyInputtedMultiplier : Exception
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public IncorrectlyInputtedMultiplier()
        {

        }
    }
}
