﻿using System;

namespace ExceptionLibrary
{
    /// <summary>
    /// Класс описывающий исключение для некорректно заполненных полей полиномов
    /// </summary>
    public class InvalidPolynomialInputFormat : Exception
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public InvalidPolynomialInputFormat()
        {

        }
    }
}
