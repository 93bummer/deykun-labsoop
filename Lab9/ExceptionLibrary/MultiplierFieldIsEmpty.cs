﻿using System;

namespace ExceptionLibrary
{
    /// <summary>
    /// Класс описывающий исключение для незаполненного поля множителя
    /// </summary>
    public class MultiplierFieldIsEmpty : Exception
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public MultiplierFieldIsEmpty()
        {

        }
    }
}
