﻿using System;

namespace ExceptionLibrary
{
    /// <summary>
    /// Класс описывающий исключение для незаполненных полей полиномов
    /// </summary>
    public class PolynomialFieldIsEmpty : Exception
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public PolynomialFieldIsEmpty()
        {

        }
    }
}
