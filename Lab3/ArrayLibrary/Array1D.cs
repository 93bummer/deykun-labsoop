﻿using System;

namespace ArrayLibrary
{
    /// <summary>
    /// Класс описывающий одномерный массив
    /// </summary>
    public class Array1D
    {
        /// <summary>
        /// Массив вещественных чисел
        /// </summary>
        private double[] array;
        /// <summary>
        /// Свойство для определения длины массива
        /// </summary>
        public int ArrayLength {
            get
            {
                return array.Length;
            }
        }
        /// <summary>
        /// Индексатор для доступам к элементам поля-массива
        /// </summary>
        /// <param name="index">Индекс элемента массива</param>
        /// <returns>Элемент массива</returns>
        public double this[int index]
        {
            get
            {
                return array[index];
            }
            set
            {
                array[index] = value;
            }
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="ArrayLength">Длина массива</param>
        public Array1D(int ArrayLength)
        {
            array = new double[ArrayLength];
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="ArrayLength">Длина массива</param>
        /// <param name="array">Поле-массив вещественных чисел</param>
        public Array1D(double[] array)
        {
            this.array = array;
        }
        /// <summary>
        /// Ввод массива
        /// </summary>
        /// <param name="array">Поле-массив вещественных чисел</param>
        /// <param name="letter">Название массива</param>
        public void InputArray(char letter)
        {
            for (int i = 0; i < ArrayLength; i++)
            {
                double value;

                Console.Write(letter + "[" + i + "] = ");

                while (!double.TryParse(Console.ReadLine(), out value))
                    Console.WriteLine("Данные введены неверно, попробуйте ещё раз...");

                array[i] = value;
            }
        }
        /// <summary>
        /// Вывод массива
        /// </summary>
        /// <returns>Строка, которая содержит массив</returns>
        public override string ToString()
        {
            string value = "[";

            for(int i = 0; i < ArrayLength - 1; i++)
            {
                value += Convert.ToString(array[i]) + ", ";
            }

            value += Convert.ToString(array[ArrayLength - 1]) + "]";

            return value;
        }
        /// <summary>
        /// Метод для определения произведения всех элементов массива
        /// </summary>
        /// <param name="array">Поле-массив вещественных чисел</param>
        /// <returns>Результат произведения</returns>
        public double MultiplicationOfElements()
        {
            double value = 1;

            for (int i = 0; i < ArrayLength; i++)
            {
                value *= array[i];
            }

            return value;
        }
        /// <summary>
        /// Метод для определения произведения всех элементов массива с номерами кратными заданному числу
        /// </summary>
        /// <param name="multiple">Число, которому должны быть кратны числа</param> 
        /// <returns>Результат произведения</returns>
        public string MultiplicationOfElements(int multiple)
        {
            double value = 1;
            int counter = 0;

            /* Console.Write("Введите число определяющее крастность: ");

            while (!int.TryParse(Console.ReadLine(), out multiple) || multiple < 1)
            {
                Console.WriteLine("Данные введены некорректно, попробуйте ещё раз...");
                Console.Write("Введите число определяющее крастность: ");
            } */

            for (int i = 0; i < ArrayLength; i++)
            {
                if ((i + 1) % multiple == 0)
                {
                    value *= array[i];
                    counter++;
                }
            }

            if (counter == 0)
                return "Нет чисел кратных " + multiple;

            return Convert.ToString(value);
        }
        /// <summary>
        /// Метод для определения произведения всех элементов массива до элемента с заданным номером
        /// </summary>
        /// <param name="array">Поле-массив вещественных чисел</param>
        /// /// <param name="border">Число-граница для индексов массива</param> 
        /// <returns>Результат произведения</returns>
        public double MultiplicationOfElements(Array1D array, int border)
        {
            double value = 1;

            Console.Write("Введите число определяющее границу: ");

            while (!int.TryParse(Console.ReadLine(), out border) || border <= 0 || border > array.ArrayLength)
            {
                Console.WriteLine("Данные введены некорректно, попробуйте ещё раз...");
                Console.Write("Введите число определяющее границу: ");
            }

            for (int i = 0; i < border; i++)
            {
                value *= array[i];
            }

            return value;
        }
        /// <summary>
        /// Операция поэлементного вычитания массивов
        /// </summary>
        /// <param name="firstArr">Первый массив</param>
        /// <param name="secondArr">Второй массив</param>
        /// <returns>Новый массив</returns>
        public static Array1D operator -(Array1D firstArr, Array1D secondArr)
        {
            int newArrayLength, i;
            double[] newArray;

            if (firstArr.ArrayLength > secondArr.ArrayLength)
                newArrayLength = firstArr.ArrayLength;
            else
                newArrayLength = secondArr.ArrayLength;

            newArray = new double[newArrayLength];    

            if (firstArr.ArrayLength > secondArr.ArrayLength)
            {
                for (i = 0; i < secondArr.ArrayLength; i++)
                    newArray[i] = firstArr[i] - secondArr[i];

                for (; i < firstArr.ArrayLength; i++)
                    newArray[i] = firstArr[i];
            }
            else
            {
                if (firstArr.ArrayLength < secondArr.ArrayLength)
                {
                    for (i = 0; i < firstArr.ArrayLength; i++)
                        newArray[i] = firstArr[i] - secondArr[i];

                    for (; i < secondArr.ArrayLength; i++)
                        newArray[i] = -secondArr[i];
                }
                else
                {
                    for (i = 0; i < firstArr.ArrayLength; i++)
                        newArray[i] = firstArr[i] - secondArr[i];
                }
            }
            
            

            return new Array1D(newArray);
        }
        /// <summary>
        /// Операция сравнения массивов
        /// </summary>
        /// <param name="firstArr">Первый массив</param>
        /// <param name="secondArr">Второй массив</param>
        /// <returns>True, если массивы равны и false, если массивы не равны</returns>
        public static bool operator ==(Array1D firstArr, Array1D secondArr)
        {
            if (firstArr.ArrayLength != secondArr.ArrayLength)
                return false;

            for (int i = 0; i < firstArr.ArrayLength; i++)
                if (firstArr[i] != secondArr[i])
                    return false;

            return true;
        }
        /// <summary>
        /// Операция сравнения массивов
        /// </summary>
        /// <param name="firstArr">Первый массив</param>
        /// <param name="secondArr">Второй массив</param>
        /// <returns>False, если массивы равны и True, если массивы не равны</returns>
        public static bool operator !=(Array1D firstArr, Array1D secondArr)
        {
            return firstArr == secondArr ? false : true;
        }
    }
}