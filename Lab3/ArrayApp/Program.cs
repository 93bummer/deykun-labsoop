﻿using System;
using ArrayLibrary;

namespace ArrayApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Array1D A, B, C;
            int arrayLength = 0;

            InputArrayLength(ref arrayLength);
            A = new Array1D(arrayLength);
            A.InputArray('A');

            InputArrayLength(ref arrayLength);
            B = new Array1D(arrayLength);
            B.InputArray('B');

            InputArrayLength(ref arrayLength);
            C = new Array1D(arrayLength);
            C.InputArray('C');

            Console.WriteLine("A = " + A);
            Console.WriteLine("B = " + B);
            Console.WriteLine("C = " + C);

            Console.WriteLine("Произведение элементов массива А с чётными номерами: " + A.MultiplicationOfElements(2));
            Console.WriteLine("Произведение элементов массива C с чётными номерами: " + C.MultiplicationOfElements(2));
            Console.WriteLine("Произведение элементов массива B с номерами, которые кратны трём: " + B.MultiplicationOfElements(3));

            Console.WriteLine("\nC - A = " + (C - A));
            Console.WriteLine("A - C = " + (A - C));
            Console.WriteLine("A - B - C = " + (A - B - C));

            if (A != B)
                Console.WriteLine("\n" + GenerateArray(A, B));

            Console.ReadKey();
        }
        /// <summary>
        /// Метод ввода размерности массива
        /// </summary>
        /// <param name="arrayLength">Размерность массива</param>
        static void InputArrayLength(ref int arrayLength)
        {
            int value;

            Console.Write("Введите размерность массива:");
            
            while(!int.TryParse(Console.ReadLine(), out value) || value <= 0)
            {
                Console.WriteLine("Данные введены неверно, попробуйте ещё раз...");
                Console.Write("Введите размерность массива:");
            }
                
            arrayLength = value;
        }
        /// <summary>
        /// Метод создания нового массива из элементов первого, если они больше произведения половины элементов второго массива
        /// </summary>
        /// <param name="firstArr">Первый массив</param>
        /// <param name="secondArr">Второй массив</param>
        /// <returns>Новый массив</returns>
        static string GenerateArray(Array1D firstArr, Array1D secondArr)
        {
            int arrayLength, j = 0;
            double[] array;
            double border = 1;
            int counter = 0;

            for (int i = 0; i < secondArr.ArrayLength / 2; i++)
                border *= secondArr[i];

            for (int i = 0; i < firstArr.ArrayLength; i++)
                if (firstArr[i] > border)
                    counter++;

            if (counter > 0)
            {
                arrayLength = counter;
                array = new double[counter];

                for (int i = 0; i < firstArr.ArrayLength; i++)
                    if (firstArr[i] > border)
                    {
                        array[j] = firstArr[i];
                        j++;
                    }     
            }
            else
                return "Искомого массива несуществует";

            return Convert.ToString(new Array1D(array));
        }
    }
}