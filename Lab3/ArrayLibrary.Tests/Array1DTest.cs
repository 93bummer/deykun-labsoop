﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ArrayLibrary.Tests
{
    [TestClass]
    public class Array1DTest
    {
        [TestMethod]
        public void MultiplicationOfElements_Method_Test1()
        {
            int arrayLength = 4;
            double value = 1;
            double[] array = new double[] { 1.1, -4.22, 5, -10};

            Array1D testArray = new Array1D(array);

            foreach (double i in array)
                value *= i;

            Console.WriteLine(testArray.MultiplicationOfElements());

            if (Math.Abs(testArray.MultiplicationOfElements()) - value < 0.000001)
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void MultiplicationOfElements_Method_Test2()
        {
            int arrayLength = 6;
            double value = 1;
            double[] array = new double[] { 2.24, 10.456, -0.123, -10, 23.098, 0.345 };

            Array1D testArray = new Array1D(array);

            foreach (double i in array)
                value *= i;

            if (Math.Abs(testArray.MultiplicationOfElements()) - value < 0.000001)
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void MultiplicationOfElements_Method_Test3()
        {
            int arrayLength = 5;
            double value = 1;
            double[] array = new double[] { 1, 2, 4, 0.0000000001, 1, 0 };

            Array1D testArray = new Array1D(array);

            foreach (double i in array)
                value *= i;

            if (Math.Abs(testArray.MultiplicationOfElements()) - value < 0.000001)
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void Operator_Minus_Method_Test1()
        {
            int firstArrayLength = 3, secondArrayLength = 2, resultArrayLength;
            double[] firstArray = new double[] { 1.345, -2.123, 4.001 };
            double[] secondArray = new double[] { 0.1, -10 };

            Array1D firstTestArray = new Array1D(firstArray);
            Array1D secondTestArray = new Array1D(secondArray);

            Array1D resultArray = firstTestArray - secondTestArray;

            if (firstArrayLength > secondArrayLength)
                resultArrayLength = firstArrayLength;
            else
                resultArrayLength = secondArrayLength;

            Assert.AreEqual(resultArrayLength, resultArray.ArrayLength);
        }

        [TestMethod]
        public void Operator_Minus_Method_Test2()
        {
            int firstArrayLength = 5, secondArrayLength = 3, resultArrayLength;
            double[] firstArray = new double[] { -3.506, 123, 345.657, -0.435346, 1 };
            double[] secondArray = new double[] { 0.1, -10, 6.34 };

            Array1D firstTestArray = new Array1D(firstArray);
            Array1D secondTestArray = new Array1D(secondArray);

            Array1D resultArray = firstTestArray - secondTestArray;

            if (firstArrayLength > secondArrayLength)
                resultArrayLength = firstArrayLength;
            else
                resultArrayLength = secondArrayLength;

            Assert.AreEqual(resultArrayLength, resultArray.ArrayLength);
        }
    }
}
