﻿using System;
using System.Text;
using StringLibrary;

namespace StringApp
{
    class Program
    {
        static void Main(string[] args)
        {
            StringParser String;
            string someString;

            //Console.Write("Введите строку: ");
            someString = "Почта для готовых лабораторных работ - zevs@gmail.com. " +
                "Лабораторные работы можете брать с портала учебного портала. " +
                "Сайт edu.gstu.by. Контакты некоторых наших сотрудиков - qwerty@mail.ru, asd@tut.by и т.д. " +
                "По всем вопросам обращаться к преподавателю.";

            String = new StringParser(someString);

            Console.WriteLine("Исходная строка: " + someString);

            Console.Write("Строка в алфавитном порядке: ");

            foreach (string i in String.InAlphabetOrder(5).ToArray())
                Console.Write(i + " ");

            Console.WriteLine("\n\nПредложения, которые не содержат e-mail адреса: ");

            foreach (StringBuilder i in String.ReturnWithoutEmails())
                Console.WriteLine(i);

            String.ReturnWithoutEmails();

            Console.ReadKey();
        }
    }
}
