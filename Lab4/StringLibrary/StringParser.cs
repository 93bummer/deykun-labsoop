﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace StringLibrary
{
    public class StringParser
    {
        /// <summary>
        /// Строка
        /// </summary>
        private string someString;
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="someString">Строка</param>
        public StringParser(string someString) {
            this.someString = someString;
        }
        /// <summary>
        /// Метод, который находит все слова заданной длинны и упорядочивает их в алфавитном порядке.
        /// </summary>
        /// <param name="length">Длина слова</param>
        /// <returns>Список со словами заданной длины в алфавитном порядке</returns>
        public List<string> InAlphabetOrder(int length)
        {
            List<string> list = new List<string>();
            string[] words = Regex.Split(someString, @"[\s\d\W!.?,-]");

            foreach (string i in words)
            {
                if (i.Trim() != "" && i.Length == length)
                    list.Add(i.Trim());
            }

            list.Sort();

            return list;
        }
        /// <summary>
        /// Метод, который делит строку на предложения и выбирает из них те, в которых не содержутся e-mail адреса.
        /// </summary>
        /// <returns>Массив из строк класса StringBuilder</returns>
        public StringBuilder[] ReturnWithoutEmails()
        {
            int counterOfSentenses = 0, counterOfEmails = 0;

            StringBuilder[] value, allSentences;

            Regex sentence = new Regex(@"[А-ЯA-Z].{15,}?(\.|\!|\?)(\ |\r|\n|$)"),
                email = new Regex(@"(\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)");

            MatchCollection sentencesCounter = sentence.Matches(someString);

            allSentences = new StringBuilder[sentencesCounter.Count];

            foreach (Match i in sentencesCounter)
            {
                allSentences[counterOfSentenses] = new StringBuilder(i.Value);
                counterOfSentenses++;

                if(!email.IsMatch(i.Value))
                    counterOfEmails++;
            }

            value = new StringBuilder[counterOfEmails];

            counterOfEmails = 0;
            foreach (StringBuilder i in allSentences)
            {
                if(!email.IsMatch(i.ToString()))
                {
                    value[counterOfEmails] = new StringBuilder(i.ToString());
                    counterOfEmails++;
                }
            }

            return value;
        }
    }
}
