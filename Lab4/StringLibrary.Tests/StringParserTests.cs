﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringLibrary;

namespace StringLibrary.Tests
{
    [TestClass]
    public class StringParserTests
    {
        [TestMethod]
        public void InAlphabetOrder_Test1()
        {
            string someString = "That's a part of alphabet: g, b, e, a, ,d, f, c";
            StringParser String = new StringParser(someString);

            List<string> testList = new List<string>();
            testList.Add("a b c d e f g");

            if(String.InAlphabetOrder(1) == testList)
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void InAlphabetOrder_Test2()
        {
            string someString = "Моя семья небольшая, но очень крепкая и дружная. " +
                "В нашей семье все любят и уважают друг друга. " +
                "Нас пять человек: мама, папа, бабушка, брат и я. " +
                "У каждого из нас в семье особая роль.";
            StringParser String = new StringParser(someString);

            List<string> testList = new List<string>();
            testList.Add("брат друг папа пять роль мама");

            if (String.InAlphabetOrder(4) == testList)
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void InAlphabetOrder_Test3()
        {
            string someString = "Осень — время прощания с теплом и прихода холодов. " +
                "Дни становятся короче, ночи — длиннее, и это все заметнее с каждым новым днём. " +
                "Солнце появляется на горизонте всё позже, а заходит раньше, и день за днём греет всё слабее. " +
                "Температура на термометре за окном медленно опускается вниз, вечерами становится заметно холоднее.";
            StringParser String = new StringParser(someString);

            List<string> testList = new List<string>();
            testList.Add("время греет новым окном Осень позже");

            if (String.InAlphabetOrder(4) == testList)
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void ReturnWithoutEmails_Test1()
        {
            string someString = "Почта для готовых лабораторных работ - zevs@gmail.com. " +
                "Сайт edu.gstu.by. Контакты некоторых наших сотрудиков - qwerty@mail.ru, asd@tut.by и т.д. ";
            StringParser String = new StringParser(someString);

            StringBuilder[] testValues = new StringBuilder[0];
                
            if(testValues == String.ReturnWithoutEmails())
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void ReturnWithoutEmails_Test2()
        {
            bool isTrue = true;

            string someString = "Почта для готовых лабораторных работ - zevs@gmail.com. " +
                "Лабораторные работы можете брать с портала учебного портала. " +
                "Сайт edu.gstu.by. " +
                "Контакты некоторых наших сотрудиков - qwerty@mail.ru, asd@tut.by и т.д. " +
                "По всем вопросам обращаться к преподавателю.";
            StringParser String = new StringParser(someString);

            StringBuilder[] testValues = new StringBuilder[3],
                realMethodValue;
            testValues[0] = new StringBuilder("Лабораторные работы можете брать с портала учебного портала. ");
            testValues[1] = new StringBuilder("Сайт edu.gstu.by. ");
            testValues[2] = new StringBuilder("По всем вопросам обращаться к преподавателю.");

            realMethodValue = String.ReturnWithoutEmails();

            for (int i = 0; i < 3; i++)
            {
                if (!(testValues[i].Equals(realMethodValue[i])))
                    isTrue = false;     
            }

            if (isTrue)
                Assert.IsTrue(true);
            else
                Assert.IsTrue(false);
        }

        [TestMethod]
        public void ReturnWithoutEmails_Test3()
        {
            bool isTrue = true;

            string someString = "Это всё почта - trewq@gmail.com, dsa@tut.by и т.д. " +
                "Это не почта - trewq@, dsa@, @dsa, @trewq.";
            StringParser String = new StringParser(someString);

            StringBuilder[] testValues = new StringBuilder[1],
                realMethodValue;
            testValues[0] = new StringBuilder("Это не почта - trewq@, dsa@, @dsa, @trewq.");

            realMethodValue = String.ReturnWithoutEmails();

            if (!(testValues[0].Equals(realMethodValue[0])))
                isTrue = false;

            if (isTrue)
                Assert.IsTrue(true);
            else
                Assert.IsTrue(false);
        }
    }
}
