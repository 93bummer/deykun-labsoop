﻿using System;

namespace FigureLibrary
{
    /// <summary> 
    /// Главный класс.
    /// Содержит в себе методы для взаимодействия с криволинейной трапецией заданной на интервале, образуемой функцией cos(x) и осью Ox.
    /// </summary>
    /// <remarks>
    /// Осуществляет методы вычисления параметров криволинейной трапеции и проверки принадлежности точки, 
    /// заданной своими координатами, криволинейной трапеции и её границам.
    /// </remarks>
    public class Cosine
    {
        /// <summary>
        /// Начальная координата интервала.
        /// </summary>
        public double StartX { get; }
        /// <summary>
        /// Конечная координата интервала.
        /// </summary>
        public double EndX { get; }
        /// <summary>
        /// Свойство определяющее существование криволинейной трапеции.
        /// </summary>
        public bool IsTrulyExist { get; }
        /// <summary>
        /// Свойство определяющее принадлежность точки к криволинейной трапеции.
        /// </summary>
        public bool IsTrulyInside { get; private set; }

        /// <summary>
        /// Метод, который создаёт криволинейную трапецию.
        /// </summary>
        /// <param name="StartX">Начальная координата границы по Ox.</param>
        /// <param name="EndX">Конечная координата границы по Ox.</param>
        public Cosine(double StartX, double EndX)
        {
            if(StartX > EndX)
            {
                (StartX, EndX) = (EndX, StartX);
            }

            this.StartX = StartX;
            this.EndX = EndX;

            IsTrulyExist = IsExist();
        }

        /// <summary>
        /// Метод, который принимает значение, определяющее принадлежность точки к криволинейной трапеции.
        /// </summary>
        /// <param name="cordX">Координата точки по Ox.</param>
        /// <param name="cordY">Координата точки по Oy.</param>
        public void IsPointInside(double cordX, double cordY)
        {
            IsTrulyInside = CheckPoint(cordX, cordY); 
        }

        /// <summary>
        /// Возвращает строку, которая описывает параметры фигуры.
        /// </summary>
        /// <returns>Параметры фигуры.</returns>
        public override string ToString()
        {
            return "P = " + CalcP() + "\n"
                 + "S = " + CalcS();
        }

        /// <summary>
        /// Проверяет криволинейную трапецию на существование.
        /// </summary>
        /// <returns>True, если фигура существует и false, если не существует.</returns>
        private bool IsExist()
        {
            return (EndX - StartX < Math.PI)
                && (Math.Sign(Math.Cos(StartX)) == Math.Sign(Math.Cos(EndX)))
                && (StartX != EndX); 
        }

        /// <summary>
        /// Высчитывает периметр фигуры.
        /// </summary>
        /// <returns>Периметр криволинейной трапеции.</returns>
        private double CalcP()
        {
            int count = 10;
            double eps = 0.01, I1, 
                I2 = Integral(count);

            do
            {
                I1 = I2;
                count *= 2;
                I2 = Integral(count);
            } while ( Math.Abs(I2 - I1) > eps );


            return I2 + (EndX - StartX) + Math.Abs(Math.Cos(StartX)) + Math.Abs(Math.Cos(EndX));
        }

        /// <summary>
        /// Высчитывает площадь фигуры.
        /// </summary>
        /// <returns>Площадь криволинейной трапеции.</returns>
        private double CalcS()
        {
            return Math.Abs(Math.Sin(EndX) - Math.Sin(StartX));
        }

        /// <summary>
        /// Высчитывает интеграл методом центральных прямоугольников.
        /// </summary>
        /// <param name="count">Количество прямоугольников, на которые делится трапеция</param>
        /// <returns>Результат вычислений интеграла.</returns>
        private double Integral(int count)
        {
            double sum = 0;
            for(int i = 0; i < count; i++)
            {
                sum += Math.Cos(StartX + ((EndX - StartX) / count) * i) * ((EndX - StartX) / count);
            }
            return sum;
        }

        /// <summary>
        /// Проверяет принадлежность точки к криволинейной трапеции.
        /// </summary>
        /// <param name="cordX">Координата точки по Ox.</param>
        /// <param name="cordY">Координата точки по Oy.</param>
        /// <returns>True, если точка принадлежит и false, если не принадлежит.</returns>
        private bool CheckPoint(double cordX, double cordY)
        {
            return ((cordX >= StartX) && (cordX <= EndX)) 
                && (Math.Sign(Math.Cos(cordX)) * cordY >= 0 && Math.Sign(Math.Cos(cordX)) * cordY <= Math.Abs(Math.Cos(cordX)));
        }
    }
}
