﻿using System;
using System.Globalization;
using FigureLibrary;

namespace FigureApp
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberFormatInfo numberFormatInfo = new NumberFormatInfo()
            {
                NumberDecimalSeparator = ".",
            };

            Cosine cosine;
            double startX, endX, cordX, cordY;
            string caseNumber;
            Console.WriteLine("Введите координаты промежутка на числовой прямой, в пределах которого будет существовать криволинейная трапеция на графике косинуса.");

            do
            {
                Console.Write("Введите начальную границу: ");
                startX = ValidationCheck(numberFormatInfo);
                Console.Write("Введите конечную границу: ");
                endX = ValidationCheck(numberFormatInfo);

                cosine = new Cosine(startX, endX);

                if(!cosine.IsTrulyExist)
                    Console.WriteLine("Такая криволинейная трапеция не существует.\nПожалуйста, введите координаты границ ещё раз.");
                else
                    Console.WriteLine("Криволинейная трапеция успешно задана.");

                Console.WriteLine("Нажмите Enter, чтобы продолжить...");
                Console.ReadKey();
                Console.Clear();
            } while (!cosine.IsTrulyExist);

            do
            {
                Console.Clear();
                Console.WriteLine("----------------------------------------------------Меню----------------------------------------------------");
                Console.WriteLine("1. Вычислить площадь и периметр криволинейной трапеции.");
                Console.WriteLine("2. Задать точку и проверить принадлежит ли она криволинейной трапециии включая её границы.");
                Console.WriteLine("3. Выход.\n");

                Console.Write("Выберите пункт меню: ");
                caseNumber = Console.ReadLine();

                switch (caseNumber)
                {
                    case "1":
                        Console.WriteLine(cosine);
                        break;
                    case "2":
                        Console.Write("Введите координату по оси x: ");
                        cordX = ValidationCheck(numberFormatInfo);
                        Console.Write("Введите координату по оси y: ");
                        cordY = ValidationCheck(numberFormatInfo);
                        cosine.IsPointInside(cordX, cordY);

                        if(cosine.IsTrulyInside)
                            Console.WriteLine("Точка принадлежит.");
                        else
                            Console.WriteLine("Точка не принадлежит.");
                        break;
                    case "3":
                        break;
                    default:
                        Console.WriteLine("Введённого вами пункта меню не существует.");
                        break;
                }
                Console.WriteLine("Нажмите Enter, чтобы продолжить...");
                Console.ReadKey();
            } while (caseNumber != "3");

            Console.ReadKey();
        }

        /// <summary>
        /// Запршивает и проверяет значение введённое пользователем на корректность.
        /// </summary>
        /// <returns>Проверенное значение введённое пользователем.</returns>
        static double ValidationCheck(NumberFormatInfo numberFormatInfo)
        {
            string input;
            double value = 0;
            bool correct;

            do
            {
                correct = true;
                input = Console.ReadLine();
                try
                {
                    value = double.Parse(input, numberFormatInfo);
                }
                catch (Exception)
                {
                    Console.Write("Недопустимое значение, введите данные повторно: ");
                    correct = false;
                }
            } while (!correct);

            return value;
        }
    }
}
