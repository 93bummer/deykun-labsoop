﻿using System;
using ArithmeticStringParser;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ArithmeticStringLibraryTests
{
    [TestClass]
    public class ArithmeticExpressionTests
    {
        [TestMethod]
        public void GetCountedExpression_Test1()
        {
            double eps = 0.00001, testValue = -2.05866664819456;
            string testExpression = "5+cos(50)+sin(12)-8+cos(-50)-sin(101)";
            ArithmeticExpression expression = new ArithmeticExpression(testExpression);

            if (Math.Abs(expression.GetCountedExpression() - testValue) < eps)
                Assert.IsTrue(true);
            else
                Assert.IsTrue(false);
        }

        [TestMethod]
        public void GetCountedExpression_Test2()
        {
            double eps = 0.00001, testValue = 11.3357146257598;
            string testExpression = "cos(4)-sin(-8)+10+cos(0)";
            ArithmeticExpression expression = new ArithmeticExpression(testExpression);

            if (Math.Abs(expression.GetCountedExpression() - testValue) < eps)
                Assert.IsTrue(true);
            else
                Assert.IsTrue(false);
        }

        [TestMethod]
        public void GetCountedExpression_Test3()
        {
            double eps = 0.00001, testValue = 48.5007734194687;
            string testExpression = "sin(5)+100-cos(1)-50+sin(0)";
            ArithmeticExpression expression = new ArithmeticExpression(testExpression);

            if (Math.Abs(expression.GetCountedExpression() - testValue) < eps)
                Assert.IsTrue(true);
            else
                Assert.IsTrue(false);
        }
    }
}
