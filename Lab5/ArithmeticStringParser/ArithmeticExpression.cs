﻿using System;
using System.Text.RegularExpressions;

namespace ArithmeticStringParser
{
    /// <summary>
    /// Класс определяющий арифметическое выражение
    /// </summary>
    public class ArithmeticExpression
    {
        /// <summary>
        /// Строка определяющая арифметическое выражение
        /// </summary>
        private string expression;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="expression"Выражение></param>
        public ArithmeticExpression(string expression)
        {
            this.expression = expression;
        }
        /// <summary>
        /// Считает все слагаемые типа double
        /// </summary>
        /// <returns>Значение выражения</returns>
        public double GetCountedExpression()
        {
            double value = 0;

            foreach (double term in ParseExpression())
            {
                value += term;
            }

            return value;
        }
        /// <summary>
        /// Парсит выражение и находит в нём слагаемые
        /// </summary>
        /// <returns>Массив слагаемых типа double</returns>
        private double[] ParseExpression()
        {
            return CountParsedExpression(Regex.Matches(expression, @"\+?-?(sin\(\+?-?\d+\))?(cos\(\+?-?\d+\))?(\d+)?"));
        }
        /// <summary>
        /// Парсит массив "строковых" слагаемых
        /// </summary>
        /// <param name="terms">Коллекция совпадений</param>
        /// <returns>Массив слагаемых типа double</returns>
        private double[] CountParsedExpression(MatchCollection terms)
        {
            int counter = 0;
            double[] value = new double[terms.Count - 1];

            Regex sinus = new Regex(@"\+?-?(sin\(\+?-?\d+\))"),
                cosine = new Regex(@"\+?-?(cos\(\+?-?\d+\))");

            foreach (Match term in terms)
            {
                if(sinus.IsMatch(term.Value))
                {
                    value[counter] = Math.Sin(Double.Parse(Regex.Match(term.Value, @"\+?-?(\d+)").Value));
                    if (new Regex(@"-sin").IsMatch(term.Value))
                        value[counter] *= -1;
                } 
                else
                {
                    if (cosine.IsMatch(term.Value))
                    {
                        value[counter] = Math.Cos(Double.Parse(Regex.Match(term.Value, @"\+?-?(\d+)").Value));
                        if (new Regex(@"-cos").IsMatch(term.Value))
                            value[counter] *= -1;
                    }
                    else
                        if(term.Value != "")
                            value[counter] = Double.Parse(term.Value);
                }
                counter++;
            }

            return value;
        }
    }
}