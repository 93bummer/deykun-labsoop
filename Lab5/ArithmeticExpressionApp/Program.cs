﻿using System;
using ArithmeticStringParser;

namespace ArithmeticExpressionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string stringExpression = "sin(5)+100-cos(1)-50+sin(0)";

            ArithmeticExpression expression = new ArithmeticExpression(stringExpression);

            Console.WriteLine("Ответ: " + stringExpression + " = " + expression.GetCountedExpression());

            Console.ReadKey();
        }
    }
}
