﻿using System;
using ZooLibrary;
using System.Windows.Forms;
using System.Drawing;

namespace PetCounter
{
    public partial class UI : Form
    {
        private Zoo Zoo;

        public UI()
        {
            InitializeComponent();

            Zoo = new Zoo();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(Zoo.Count < 50 && typesOfPets.SelectedIndex != -1)
            {
                Zoo.AddPet(typesOfPets.SelectedIndex, nameBox.Text);
                numberOfPets.Text = Convert.ToString(Zoo.Count);
                status.ForeColor = Color.FromArgb(0, 192, 0);
                status.Text = "Success!";

                listOfPets.Items.Add(Zoo.GetInfoAboutPet(Zoo.Count - 1));
            }
            else
            {
                status.ForeColor = Color.Red;
                if (Zoo.Count == 50)
                    status.Text = "List is full."; 
                else
                    status.Text = "You should to choose an item.";
            }
        }

        private void firstQuestionBtn_Click(object sender, EventArgs e)
        {
            firstAnswer.Text = Convert.ToString(Zoo.CountHouses(firstQuestion.SelectedIndex));
        }

        private void secondQuestionBtn_Click(object sender, EventArgs e)
        {
            secondAnswer.Text = Convert.ToString(Zoo.CountPetsByTypeOfSkin(secondQuestion.SelectedIndex));
        }

        private void thirdQuestionBtn_Click(object sender, EventArgs e)
        {
            thirdAnswer.Text = Convert.ToString(Zoo.CountPetsByThemType(thirdQuestion.SelectedIndex));
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (listOfPets.SelectedIndex >= 0 && listOfPets.SelectedIndex < Zoo.Count)
            {
                Zoo.DeletePet(listOfPets.SelectedIndex);
                listOfPets.Items.Clear();

                for (int i = 0; i < Zoo.Count; i++)
                    listOfPets.Items.Add(Zoo.GetInfoAboutPet(i));

                numberOfPets.Text = Convert.ToString(Zoo.Count);
                status.ForeColor = Color.Green;
                status.Text = "Success!";
            }
            else
            {
                status.ForeColor = Color.Red;
                status.Text = "Select list item.";
            }
        }
    }
}
