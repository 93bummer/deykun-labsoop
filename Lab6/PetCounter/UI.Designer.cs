﻿namespace PetCounter
{
    partial class UI
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.firstQuestionBtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.secondQuestionBtn = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.thirdQuestionBtn = new System.Windows.Forms.Button();
            this.firstAnswer = new System.Windows.Forms.TextBox();
            this.secondAnswer = new System.Windows.Forms.TextBox();
            this.thirdAnswer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numberOfPets = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.status = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.listOfPets = new System.Windows.Forms.ListBox();
            this.typesOfPets = new System.Windows.Forms.ComboBox();
            this.firstQuestion = new System.Windows.Forms.ComboBox();
            this.secondQuestion = new System.Windows.Forms.ComboBox();
            this.thirdQuestion = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.s = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(18, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Choose a type of pet:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(123, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "This programm counts pets.";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(20, 161);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(120, 30);
            this.btnAdd.TabIndex = 7;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 224);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(215, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "How many pets need a                               ?";
            // 
            // firstQuestionBtn
            // 
            this.firstQuestionBtn.Location = new System.Drawing.Point(19, 250);
            this.firstQuestionBtn.Name = "firstQuestionBtn";
            this.firstQuestionBtn.Size = new System.Drawing.Size(84, 23);
            this.firstQuestionBtn.TabIndex = 11;
            this.firstQuestionBtn.Text = "Show answer";
            this.firstQuestionBtn.UseVisualStyleBackColor = true;
            this.firstQuestionBtn.Click += new System.EventHandler(this.firstQuestionBtn_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 300);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(197, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "How many pets have                            ?";
            // 
            // secondQuestionBtn
            // 
            this.secondQuestionBtn.Location = new System.Drawing.Point(21, 325);
            this.secondQuestionBtn.Name = "secondQuestionBtn";
            this.secondQuestionBtn.Size = new System.Drawing.Size(84, 23);
            this.secondQuestionBtn.TabIndex = 11;
            this.secondQuestionBtn.Text = "Show answer";
            this.secondQuestionBtn.UseVisualStyleBackColor = true;
            this.secondQuestionBtn.Click += new System.EventHandler(this.secondQuestionBtn_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 366);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(190, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "How many                           in the zoo?";
            // 
            // thirdQuestionBtn
            // 
            this.thirdQuestionBtn.Location = new System.Drawing.Point(20, 398);
            this.thirdQuestionBtn.Name = "thirdQuestionBtn";
            this.thirdQuestionBtn.Size = new System.Drawing.Size(84, 23);
            this.thirdQuestionBtn.TabIndex = 11;
            this.thirdQuestionBtn.Text = "Show answer";
            this.thirdQuestionBtn.UseVisualStyleBackColor = true;
            this.thirdQuestionBtn.Click += new System.EventHandler(this.thirdQuestionBtn_Click);
            // 
            // firstAnswer
            // 
            this.firstAnswer.Location = new System.Drawing.Point(111, 253);
            this.firstAnswer.Name = "firstAnswer";
            this.firstAnswer.Size = new System.Drawing.Size(31, 20);
            this.firstAnswer.TabIndex = 13;
            // 
            // secondAnswer
            // 
            this.secondAnswer.Location = new System.Drawing.Point(110, 328);
            this.secondAnswer.Name = "secondAnswer";
            this.secondAnswer.Size = new System.Drawing.Size(31, 20);
            this.secondAnswer.TabIndex = 13;
            // 
            // thirdAnswer
            // 
            this.thirdAnswer.Location = new System.Drawing.Point(111, 401);
            this.thirdAnswer.Name = "thirdAnswer";
            this.thirdAnswer.Size = new System.Drawing.Size(31, 20);
            this.thirdAnswer.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(160, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "Number of pets:";
            // 
            // numberOfPets
            // 
            this.numberOfPets.ForeColor = System.Drawing.Color.Black;
            this.numberOfPets.Location = new System.Drawing.Point(288, 73);
            this.numberOfPets.Name = "numberOfPets";
            this.numberOfPets.Size = new System.Drawing.Size(68, 20);
            this.numberOfPets.TabIndex = 17;
            this.numberOfPets.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(160, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 20);
            this.label4.TabIndex = 16;
            this.label4.Text = "Status:";
            // 
            // status
            // 
            this.status.ForeColor = System.Drawing.Color.Red;
            this.status.Location = new System.Drawing.Point(226, 105);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(149, 20);
            this.status.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(474, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Type of pet";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(545, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Type of skin";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(619, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Type of house";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(146, 161);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(120, 30);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // listOfPets
            // 
            this.listOfPets.FormattingEnabled = true;
            this.listOfPets.Location = new System.Drawing.Point(424, 46);
            this.listOfPets.Name = "listOfPets";
            this.listOfPets.Size = new System.Drawing.Size(278, 368);
            this.listOfPets.TabIndex = 27;
            // 
            // typesOfPets
            // 
            this.typesOfPets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typesOfPets.FormattingEnabled = true;
            this.typesOfPets.Items.AddRange(new object[] {
            "Cat",
            "Parrot",
            "Fish"});
            this.typesOfPets.Location = new System.Drawing.Point(20, 72);
            this.typesOfPets.Name = "typesOfPets";
            this.typesOfPets.Size = new System.Drawing.Size(121, 21);
            this.typesOfPets.TabIndex = 28;
            // 
            // firstQuestion
            // 
            this.firstQuestion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.firstQuestion.FormattingEnabled = true;
            this.firstQuestion.Items.AddRange(new object[] {
            "Cell",
            "Terrarium",
            "Aquarium"});
            this.firstQuestion.Location = new System.Drawing.Point(133, 221);
            this.firstQuestion.Name = "firstQuestion";
            this.firstQuestion.Size = new System.Drawing.Size(87, 21);
            this.firstQuestion.TabIndex = 29;
            // 
            // secondQuestion
            // 
            this.secondQuestion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.secondQuestion.FormattingEnabled = true;
            this.secondQuestion.Items.AddRange(new object[] {
            "Wool",
            "Feathers",
            "Scales"});
            this.secondQuestion.Location = new System.Drawing.Point(124, 296);
            this.secondQuestion.Name = "secondQuestion";
            this.secondQuestion.Size = new System.Drawing.Size(80, 21);
            this.secondQuestion.TabIndex = 29;
            // 
            // thirdQuestion
            // 
            this.thirdQuestion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.thirdQuestion.FormattingEnabled = true;
            this.thirdQuestion.Items.AddRange(new object[] {
            "Cats",
            "Parrots",
            "Fishes"});
            this.thirdQuestion.Location = new System.Drawing.Point(73, 362);
            this.thirdQuestion.Name = "thirdQuestion";
            this.thirdQuestion.Size = new System.Drawing.Size(76, 21);
            this.thirdQuestion.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "label9";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Write the name:";
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(21, 116);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(119, 20);
            this.nameBox.TabIndex = 32;
            // 
            // s
            // 
            this.s.AutoSize = true;
            this.s.Location = new System.Drawing.Point(424, 27);
            this.s.Name = "s";
            this.s.Size = new System.Drawing.Size(35, 13);
            this.s.TabIndex = 33;
            this.s.Text = "Name";
            // 
            // UI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 438);
            this.Controls.Add(this.s);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.thirdQuestion);
            this.Controls.Add(this.secondQuestion);
            this.Controls.Add(this.firstQuestion);
            this.Controls.Add(this.typesOfPets);
            this.Controls.Add(this.listOfPets);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.status);
            this.Controls.Add(this.numberOfPets);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.thirdAnswer);
            this.Controls.Add(this.secondAnswer);
            this.Controls.Add(this.firstAnswer);
            this.Controls.Add(this.thirdQuestionBtn);
            this.Controls.Add(this.secondQuestionBtn);
            this.Controls.Add(this.firstQuestionBtn);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "UI";
            this.Text = "PetsCounter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button firstQuestionBtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button secondQuestionBtn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button thirdQuestionBtn;
        private System.Windows.Forms.TextBox firstAnswer;
        private System.Windows.Forms.TextBox secondAnswer;
        private System.Windows.Forms.TextBox thirdAnswer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox numberOfPets;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox status;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ListBox listOfPets;
        private System.Windows.Forms.ComboBox typesOfPets;
        private System.Windows.Forms.ComboBox firstQuestion;
        private System.Windows.Forms.ComboBox secondQuestion;
        private System.Windows.Forms.ComboBox thirdQuestion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label s;
    }
}

