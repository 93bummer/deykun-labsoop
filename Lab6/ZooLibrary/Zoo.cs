﻿/// <summary>
/// Учёт зоопарка
/// </summary>
namespace ZooLibrary
{
    /// <summary>
    /// Класс определяющий зоопарк
    /// </summary>
    public class Zoo
    {
        /// <summary>
        /// Свойство позволяющее получить доступ к массиву домашних животных
        /// </summary>
        private Pet[] Pets { get; set; }
        /// <summary>
        /// Свойство позволяющее получить доступ к количеству домашних животных
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public Zoo()
        {
            Pets = new Pet[50];
            Count = 0;
        }
        /// <summary>
        /// Метод, который добавляет домашнее животное
        /// </summary>
        /// <param name="typeOfPet">Тип питомца</param>
        public void AddPet(int typeOfPet, string name)
        {
            Pets[Count] = new Pet(typeOfPet, name);
            Count++;
        }
        /// <summary>
        /// Метод, который удаляет выбранное домашнее животное
        /// </summary>
        /// <param name="index"></param>
        public void DeletePet(int index)
        {
            for (int i = index; i < Count; i++)
                Pets[i] = Pets[i + 1];

            Count--;
        }
        public string GetInfoAboutPet(int index)
        {
            return Pets[index].NameOfPet + "\t" + Pets[index].TypeOfPet + "\t" + Pets[index].TypeOfSkin + "\t" + Pets[index].TypeOfHouse;
        }
        /// <summary>
        /// Функция, которая считает количество домашних животных по типу жилища
        /// </summary>
        /// <param name="typeOfHouse">Тип жилища домашнего животного</param>
        /// <returns>Количество домашних животных с заданным типом жилища</returns>
        public int CountHouses(int typeOfHouse)
        {
            return CountByСondition(typeOfHouse, "typeOfHouse");
        }
        /// <summary>
        /// Функция, которая считает количество домашних животных по типу кожного покрова
        /// </summary>
        /// <param name="typeOfSkin">Тип кожного покрова домашнего животного</param>
        /// <param name="pets">Массив домашних животных</param>
        /// <param name="count">Количество домашних животных</param>
        /// <returns>Количество домашних животных с заданным типом кожного покрова</returns>
        public int CountPetsByTypeOfSkin(int typeOfSkin)
        {
            return CountByСondition(typeOfSkin, "typeOfSkin");
        }
        /// <summary>
        /// Функция, которая считает количество домашних животных по типу домашнего животного
        /// </summary>
        /// <param name="typeOfPet">Тип домашнего животного</param>
        /// <returns>Количество домашних животных с заданным типом домашнего животного</returns>
        public int CountPetsByThemType(int typeOfPet)
        {
            return CountByСondition(typeOfPet, "typeOfPet");
        }
        /// <summary>
        /// Функция, которая считает количество домашних животных по заданному типу
        /// </summary>
        /// <param name="type">Тип домашнего животного</param>
        /// <param name="condition">Тип сравнения</param>
        /// <returns>Количество домашних животных с заданным типом</returns>
        private int CountByСondition(int type, string condition)
        {
            int value = 0;

            for (int i = 0; i < Count; i++)
                switch (condition)
                {
                    case "typeOfHouse":
                        if ((int)Pets[i].TypeOfHouse == type)
                            value++;
                        break;
                    case "typeOfSkin":
                        if ((int)Pets[i].TypeOfSkin == type)
                            value++;
                        break;
                    case "typeOfPet":
                        if ((int)Pets[i].TypeOfPet == type)
                            value++;
                        break;
                }

            return value;
        }
        
        /// <summary>
        /// Вложенный класс определяющий домашнее животное
        /// </summary>
        private class Pet
        {
            /// <summary>
            /// Тип животного
            /// </summary>
            public enum Pets
            {
                /// <summary>
                /// Кот\Кошка
                /// </summary>
                Cat,
                /// <summary>
                /// Попугай
                /// </summary>
                Parrot,
                /// <summary>
                /// РЫба
                /// </summary>
                Fish
            }
            /// <summary>
            /// Тип кожи
            /// </summary>
            public enum Skin
            {
                /// <summary>
                /// Шерсть
                /// </summary>
                Wool,
                /// <summary>
                /// Перья
                /// </summary>
                Feathers,
                /// <summary>
                /// Чешуя
                /// </summary>
                Scales
            }
            /// <summary>
            /// Тип жилища
            /// </summary>
            public enum House
            {
                /// <summary>
                /// Клетка
                /// </summary>
                Cell,
                /// <summary>
                /// Террариум
                /// </summary>
                Terrarium,
                /// <summary>
                /// Аквариум
                /// </summary>
                Aquarium
            }

            /// <summary>
            /// Тип домашнего животного
            /// </summary>
            public Pets TypeOfPet { get; private set; }
            /// <summary>
            /// Тип кожного покрова домашнего животного
            /// </summary>
            public Skin TypeOfSkin { get; private set; }
            /// <summary>
            /// Тип жилища домашнего животного
            /// </summary>
            public House TypeOfHouse { get; private set; }
            /// <summary>
            /// Имя домашнего животного
            /// </summary>
            public string NameOfPet { get; set; }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="typeOfPet">Тип домашнего животного</param>
            /// <param name="typeOfSkin">Тип кожного покрова домашнего животного</param>
            /// <param name="typeOfHouse">Тип жилища домашнего животного</param>
            public Pet(int type, string name)
            {
                this.TypeOfPet = (Pets)type;
                this.TypeOfSkin = (Skin)type;
                this.TypeOfHouse = (House)type;
                this.NameOfPet = name;
            }
        }
    }
}