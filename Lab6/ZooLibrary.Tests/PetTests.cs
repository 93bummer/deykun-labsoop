﻿using System;
using ZooLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZooLibrary.Tests
{
    [TestClass]
    public class PetTests
    {
        private Zoo Zoo;

        [TestMethod]
        public void CountHouses_Test()
        {
            Random rnd = new Random();
            int value, testValue = 0;
            Zoo = new Zoo();

            for (int i = 0; i < 50; i++)
            {
                Zoo.AddPet(rnd.Next(0, 2));

                if (Zoo.Pets[i].TypeOfHouse == 0)
                    testValue++;
            }


            value = Zoo.CountHouses(0);

            if (value == testValue)
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void CountSkin_Test()
        {
            Random rnd = new Random();
            int value, testValue = 0;
            Zoo = new Zoo();

            for (int i = 0; i < 50; i++)
            {
                Zoo.AddPet(rnd.Next(0, 2));

                if ((int)Zoo.Pets[i].TypeOfSkin == 1)
                    testValue++;
            }


            value = Zoo.CountHouses(1);

            if (value == testValue)
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void CountPet_Test()
        {
            Random rnd = new Random();
            int value, testValue = 0;
            Zoo = new Zoo();

            for (int i = 0; i < 50; i++)
            {
                Zoo.AddPet(rnd.Next(0, 2));

                if ((int)Zoo.Pets[i].TypeOfPet == 2)
                    testValue++;
            }


            value = Zoo.CountHouses(2);

            if (value == testValue)
                Assert.IsTrue(true);
        }
    }
}
