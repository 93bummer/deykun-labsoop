﻿using System;
using System.Windows;
using CarsLibrary;

namespace CarsCounter
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Garage garage;

        public MainWindow()
        {
            InitializeComponent();

            garage = new Garage();
        }

        private void ListBoxItem_Selected(object sender, RoutedEventArgs e)
        {
            freightQuestion.Visibility = Visibility;
            passengerQuestion.Visibility = Visibility.Hidden;
        }

        private void ListBoxItem_Selected_1(object sender, RoutedEventArgs e)
        {
            freightQuestion.Visibility = Visibility.Hidden;
            passengerQuestion.Visibility = Visibility;
        }

        private void ListBoxItem_Selected_2(object sender, RoutedEventArgs e)
        {
            freightQuestion.Visibility = Visibility;
            passengerQuestion.Visibility = Visibility;
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            garage.AddCar(typesOfCars.SelectedIndex, DateTime.Parse(releaseDate.SelectedDate.ToString()).ToShortDateString(), Convert.ToInt32(mileage.Text), mark.Text, Convert.ToInt32(carryingCapacity.Text), Convert.ToInt32(passengerCapacity.Text));
            tableOfCars.Items.Add(garage.GetInfoAboutCarByIndex(garage.Count - 1));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            averageCarrying.Text = garage.CountAverageCarryingCapacity().ToString();
            averageNumOfPassenger.Text = garage.CountAveragePassengerCapacity().ToString();
            averageMileageOfFreightCars.Text = garage.CountAvarageMileageOfFreightCars().ToString();
            averageMileageOfFreightPassengerCars.Text = garage.CountAverageMileageOfFreightPassengerCars().ToString();
        }
    }
}
