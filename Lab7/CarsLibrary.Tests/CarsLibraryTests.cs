﻿using System;
using CarsCounter;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarsLibrary.Tests
{
    [TestClass]
    public class CarsLibraryTests
    {
        [TestMethod]
        public void CountAverageCarryingCapacity_Test()
        {
            MainWindow mainWindow = new MainWindow();
            FreightCar[] testCars = new FreightCar[4]; 
            double testValue = 0;

            testCars[0] = new FreightCar("12.12.2019", 120, "Ford", 1200);
            testCars[1] = new FreightCar("10.01.2001", 10000, "ВАЗ", 10000);
            testCars[2] = new FreightCar("20.10.2009", 5000, "Skoda", 4250);
            testCars[3] = new FreightCar("14.06.2006", 7500, "Jeep", 15000);

            foreach (FreightCar car in testCars)
            {
                testValue += car.CarryingCapacity;

                mainWindow.freightCars.Add(car);
            }

            testValue /= 4;

            if (Math.Abs(mainWindow.CountAverageCarryingCapacity() - testValue) < 0.0001)
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void CountAveragePassengerCapacity_Test()
        {
            MainWindow mainWindow = new MainWindow();
            PassengerСar[] testCars = new PassengerСar[4];
            int testValue = 0;

            testCars[0] = new PassengerСar("12.12.2019", 120, "Ford", 25);
            testCars[1] = new PassengerСar("10.01.2001", 10000, "ВАЗ", 45);
            testCars[2] = new PassengerСar("20.10.2009", 5000, "Skoda", 12);
            testCars[3] = new PassengerСar("14.06.2006", 7500, "Jeep", 15);

            foreach (PassengerСar car in testCars)
            {
                testValue += car.PassengerCapacity;

                mainWindow.passengerCars.Add(car);
            }

            testValue /= 4;

            if (mainWindow.CountAveragePassengerCapacity() == testValue)
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void CountAvarageMileageOfFreightCars_Test()
        {
            MainWindow mainWindow = new MainWindow();
            FreightCar[] testCars = new FreightCar[4];
            double testValue = 0;

            testCars[0] = new FreightCar("12.12.2019", 120, "Ford", 1200);
            testCars[1] = new FreightCar("10.01.2001", 10000, "ВАЗ", 10000);
            testCars[2] = new FreightCar("20.10.2009", 5000, "Skoda", 4250);
            testCars[3] = new FreightCar("14.06.2006", 7500, "Jeep", 15000);

            foreach (FreightCar car in testCars)
            {
                testValue += car.Mileage;

                mainWindow.freightCars.Add(car);
            }

            testValue /= 4;

            if (Math.Abs(mainWindow.CountAvarageMileageOfFreightCars() - testValue) < 0.0001)
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void CountAverageMileageOfFreightPassengerCars_Test()
        {
            MainWindow mainWindow = new MainWindow();
            FreightPassengerCar[] testCars = new FreightPassengerCar[4];
            double testValue = 0;

            testCars[0] = new FreightPassengerCar("12.12.2019", 150, "Ford", 1200, 25);
            testCars[1] = new FreightPassengerCar("10.01.2001", 7777, "ВАЗ", 10000, 45);
            testCars[2] = new FreightPassengerCar("20.10.2009", 4922, "Skoda", 4250, 12);
            testCars[3] = new FreightPassengerCar("14.06.2006", 6723, "Jeep", 15000, 15);

            foreach (FreightPassengerCar car in testCars)
            {
                testValue += car.Mileage;

                mainWindow.freightPassengerCars.Add(car);
            }

            testValue /= 4;

            if (Math.Abs(mainWindow.CountAverageMileageOfFreightPassengerCars() - testValue) < 0.0001)
                Assert.IsTrue(true);
        }
    }
}
