﻿namespace CarsLibrary
{
    /// <summary>
    /// Класс-обёртка реализующий подсчёт автомобилей
    /// </summary>
    public class Garage
    {
        /// <summary>
        /// Массив автомобилей
        /// </summary>
        private Car[] cars;
        /// <summary>
        /// Счётчик автомобилей
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public Garage()
        {
            cars = new Car[15];
            Count = 0;
        }

        /// <summary>
        /// Метод, который добавляет автомобиль
        /// </summary>
        /// <param name="typesOfCars">Тип машины</param>
        /// <param name="releaseDate">Дата выпуска</param>
        /// <param name="mileage">Пробег</param>
        /// <param name="mark">Марка</param>
        /// <param name="carryingCapacity">Грузоподъёмность</param>
        /// <param name="passengerCapacity">Пассажироёмкость</param>
        public void AddCar(int typesOfCars, string releaseDate, int mileage, string mark, int carryingCapacity, int passengerCapacity)
        {
            switch (typesOfCars)
            {
                case 0:
                    cars[Count] = new FreightCar(releaseDate, mileage, mark, carryingCapacity);
                    break;
                case 1:
                    cars[Count] = new PassengerСar(releaseDate, mileage, mark, passengerCapacity);
                    break;
                case 2:
                    cars[Count] = new FreightPassengerCar(releaseDate, mileage, mark, carryingCapacity, passengerCapacity);
                    break;
            }

            Count++;
        }
        /// <summary>
        /// Метод, который считает среднюю грузоподёмность автомобилей
        /// </summary>
        /// <returns>Средняя грузоподъёмность</returns>
        public double CountAverageCarryingCapacity()
        {
            double value = 0;
            int count = 0;

            foreach (Car car in cars)
            {
                if(car is FreightCar)
                {
                    value += ((FreightCar)car).CarryingCapacity;
                    count++;
                }
                else
                    if(car is FreightPassengerCar)
                    {
                        value += ((FreightPassengerCar)car).CarryingCapacity;
                        count++;
                    }
            }

            if (count != 0)
                return value / count;
            else
                return 0;
        }
        /// <summary>
        /// Метод, который считает среднюю пассажироёмкость автомобилей
        /// </summary>
        /// <returns>Средняя пассажироёмкость</returns>
        public int CountAveragePassengerCapacity()
        {
            double value = 0;
            int count = 0;

            foreach (Car car in cars)
            {
                if (car is PassengerСar)
                {
                    value += ((PassengerСar)car).PassengerCapacity;
                    count++;
                }
                else
                    if (car is FreightPassengerCar)
                {
                    value += ((FreightPassengerCar)car).PassengerCapacity;
                    count++;
                }
            }

            if (count != 0)
                return (int)value / count;
            else
                return 0;
        }
        /// <summary>
        /// Метод, который считает средний пробег среди грузовых автомобилей
        /// </summary>
        /// <returns>Средний пробег</returns>
        public double CountAvarageMileageOfFreightCars()
        {
            double value = 0;
            int count = 0;

            foreach (Car car in cars)
            {
                if (car is FreightCar)
                {
                    value += ((FreightCar)car).Mileage;
                    count++;
                }
            }

            if (count != 0)
                return value / count;
            else
                return 0;
        }
        /// <summary>
        /// Метод, который считает средний пробег среди грузопассажирских автомобилей
        /// </summary>
        /// <returns>Средний пробег</returns>
        public double CountAverageMileageOfFreightPassengerCars()
        {
            double value = 0;
            int count = 0;

            foreach (Car car in cars)
            {
                if (car is FreightPassengerCar)
                {
                    value += ((FreightPassengerCar)car).Mileage;
                    count++;
                }
            }

            if (count != 0)
                return value / count;
            else
                return 0;
        }
        /// <summary>
        /// Предоставляет информацию об автомобиле по индексу
        /// </summary>
        /// <param name="index">Индекс автомобиля</param>
        /// <returns>Информация об автомобиле</returns>
        public string GetInfoAboutCarByIndex(int index)
        {
            return cars[index].GetInfo();
        }
    }
}
