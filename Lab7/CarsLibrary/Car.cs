﻿namespace CarsLibrary
{
    /// <summary>
    /// Класс определяющий машину
    /// </summary>
    public class Car
    {
        /// <summary>
        /// Поле даты пыпуска автомобиля
        /// </summary>
        protected string releaseDate;
        /// <summary>
        /// Поле пробега автомобиля
        /// </summary>
        protected int mileage;
        /// <summary>
        /// Поле марки автомобиля
        /// </summary>
        protected string mark;
        
        /// <summary>
        /// Свойство доступа к дате выпуска
        /// </summary>
        public string ReleaseDate
        {
            get
            {
                return releaseDate;
            }
        }
        /// <summary>
        /// Свойство доступа к пробегу
        /// </summary>
        public int Mileage 
        { 
            get
            {
                return mileage;
            }
        }
        /// <summary>
        /// Свойство доступа к марке
        /// </summary>
        public string Mark
        {
            get
            {
                return mark;
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="releaseDate">Дата выпуска</param>
        /// <param name="mileage">Пробег</param>
        /// <param name="mark">Марка</param>
        protected Car(string releaseDate, int mileage, string mark)
        {
            this.releaseDate = releaseDate;
            this.mileage = mileage;
            this.mark = mark;
        }
        /// <summary>
        /// Метод, который возвращает информацию об объекте
        /// </summary>
        /// <returns>Информация об объекте</returns>
        public virtual string GetInfo()
        {
            return $"{releaseDate}      {mileage}км      {mark}";
        }
    }
}
