﻿namespace CarsLibrary
{
    public class FreightPassengerCar : Car
    {
        /// <summary>
        /// Поле грузоподъёмности автомобиля
        /// </summary>
        private int carryingCapacity;
        /// <summary>
        /// Поле пассажироёмкости автомобиля
        /// </summary>
        private int passengerCapacity;

        /// <summary>
        /// Свойство типа автомобиля
        /// </summary>
        public string Type
        {
            get
            {
                return "Грузопассажирский";
            }
        }
        /// <summary>
        /// Свойство доступа к грузоподъёмности автомобиля
        /// </summary>
        public int CarryingCapacity 
        {
            get
            {
                return carryingCapacity;
            }
        }
        /// <summary>
        /// Свойство доступа к пассажироёмкости автомобиля
        /// </summary>
        public int PassengerCapacity
        { 
            get
            {
                return passengerCapacity;
            } 
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="releaseDate">Дата выпуска</param>
        /// <param name="mileage">Пробег</param>
        /// <param name="mark">Марка</param>
        /// <param name="carryingCapacity">Грузоподъёмность</param>
        /// <param name="passengerCapacity">Пассажироёмкость</param>
        public FreightPassengerCar(string releaseDate, int mileage, string mark, int carryingCapacity, int passengerCapacity)
            : base(releaseDate, mileage, mark) 
        {
            this.carryingCapacity = carryingCapacity;
            this.passengerCapacity = passengerCapacity;
        }
        /// <summary>
        /// Метод, который возвращает информацию об объекте
        /// </summary>
        /// <returns>Информация об объекте</returns>
        public override string GetInfo()
        {
            return $"{Type}      {releaseDate}      Пробег: {mileage}км     Марка: '{mark}'      Грузоподъёмность: {carryingCapacity}кг      Пассажироёмкость: {passengerCapacity} человек";
        }
    }
}
