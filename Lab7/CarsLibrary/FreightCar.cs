﻿namespace CarsLibrary
{
    /// <summary>
    /// Производный класс определяющий грузовой автомобиль
    /// </summary>
    public class FreightCar : Car
    {
        /// <summary>
        /// Поле грузоподъёмности автомобиля
        /// </summary>
        private int carryingCapacity;

        /// <summary>
        /// Свойство типа автомобиля
        /// </summary>
        public string Type
        {
            get
            {
                return "Грузовой";
            }
        }
        /// <summary>
        /// Свойство доступа к грузоподъёмности автомобиля
        /// </summary>
        public int CarryingCapacity
        {
            get
            {
                return carryingCapacity;
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="releaseDate">Дата выпуска</param>
        /// <param name="mileage">Пробег</param>
        /// <param name="mark">Марка</param>
        /// <param name="carryingCapacity">Грузоподъёмность</param>
        public FreightCar(string releaseDate, int mileage, string mark, int carryingCapacity) : base(releaseDate, mileage, mark)
        {
            this.carryingCapacity = carryingCapacity;
        }
        /// <summary>
        /// Метод, который возвращает информацию об объекте
        /// </summary>
        /// <returns>Информация об объекте</returns>
        public override string GetInfo()
        {
            return $"{Type}      {releaseDate}      Пробег: {mileage}км     Марка: '{mark}'      Грузоподъёмность: {carryingCapacity}кг";
        }
    }
}
