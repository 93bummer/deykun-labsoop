﻿namespace CarsLibrary
{
    /// <summary>
    /// Производный класс определяющий пассажирский автомобиль
    /// </summary>
    public class PassengerСar : Car
    {
        /// <summary>
        /// Поле пассажироёмкости автомобиля
        /// </summary>
        private int passengerCapacity;

        /// <summary>
        /// Свойство типа автомобиля
        /// </summary>
        public string Type
        {
            get
            {
                return "Пассажирский";
            }
        }
        /// <summary>
        /// Свойство грузоподъёмности автомобиля
        /// </summary>
        public int CarryingCapacity
        {
            get
            {
                return 0;
            }
        }
        /// <summary>
        /// Свойство доступа к пассажироёмкости автомобиля
        /// </summary>
        public int PassengerCapacity
        {
            get
            {
                return passengerCapacity;
            }
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="releaseDate">Дата выпуска</param>
        /// <param name="mileage">Пробег</param>
        /// <param name="mark">Марка</param>
        /// <param name="passengerCapacity">Пассажироёмкость</param>
        public PassengerСar(string releaseDate, int mileage, string mark, int passengerCapacity) : base(releaseDate, mileage, mark)
        {
            this.passengerCapacity = passengerCapacity;
        }
        /// <summary>
        /// Метод, который возвращает информацию об объекте
        /// </summary>
        /// <returns>Информация об объекте</returns>
        public override string GetInfo()
        {
            return $"{Type}      {releaseDate}      Пробег: {mileage}км      Марка: '{mark}'      Пассажироёмкость: {passengerCapacity} человек";
        }
    }
}
