﻿using System;
using System.Text;
using PolynomialLibrary;

namespace PolynomialCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            Polynomial firstPoly, secondPoly;
            string caseNumber;
            int multiplier;
            uint power = 0;
            double[] odds;

            Console.Write("Введите степень первого полинома: ");
            power = InputPower();
            odds = new double[power + 1];
            InputOdds(power, odds);
            firstPoly = new Polynomial(power, odds);

            Console.Write("Введите степень второго полинома: ");
            power = InputPower();
            odds = new double[power + 1];
            InputOdds(power, odds);
            secondPoly = new Polynomial(power, odds);

            Console.WriteLine("\nПервый полином: " + firstPoly);
            Console.WriteLine("Второй полином: " + secondPoly);

            Console.WriteLine("\nНажмите Enter, чтобы продолжить...");
            Console.ReadKey();

            do
            {
                Console.Clear();
                Console.WriteLine("----------------------------------------------------Меню----------------------------------------------------");
                Console.WriteLine("1. Вывести на экран введённые полиномы.");
                Console.WriteLine("2. Умножить полиномы друг на друга.");
                Console.WriteLine("3. Умножить выбранный полином на число.");
                Console.WriteLine("4. Выход.\n");

                Console.Write("Выберите пункт меню: ");
                caseNumber = Console.ReadLine();

                switch (caseNumber)
                {
                    case "1":
                        Console.WriteLine("\nПервый полином: " + firstPoly);
                        Console.WriteLine("Второй полином: " + secondPoly);
                        break;
                    case "2":
                        Console.WriteLine(firstPoly + " * " + secondPoly + " = " + firstPoly * secondPoly);
                        break;
                    case "3":
                        Console.Write("Введите множитель полинома: ");

                        while (!int.TryParse(Console.ReadLine(), out multiplier))
                        {
                            Console.WriteLine("Данные введнены некорректно, попробуйте ещё раз...");
                            Console.Write("Множитель: ");
                        }

                        do
                        {
                            Console.Clear();
                            Console.WriteLine("\n1. " + firstPoly);
                            Console.WriteLine("2. " + secondPoly);
                            Console.WriteLine("3. Выход.");
                            Console.Write("\nВыберите полином: ");
                            caseNumber = Console.ReadLine();
                            Console.WriteLine("\n");
                            switch (caseNumber)
                            {
                                case "1":
                                    Console.WriteLine(multiplier + "*(" + firstPoly + ") = " + multiplier * firstPoly);
                                    break;
                                case "2":
                                    Console.WriteLine(multiplier + "*(" + secondPoly + ") = " + multiplier * secondPoly);
                                    break;
                                case "3":
                                    break;
                                default:
                                    Console.WriteLine("Введённого вами пункта не существует.");
                                    break;
                            }
                            Console.WriteLine("\nНажмите Enter, чтобы продолжить...");
                            Console.ReadKey();
                        } while (caseNumber != "3");
                        break;
                    case "4":
                        break;
                    default:
                        Console.WriteLine("Введённого вами пункта меню не существует.");
                        break;
                }
                Console.WriteLine("\nНажмите Enter, чтобы продолжить...");
                Console.ReadKey();
            } while (caseNumber != "4");

            Console.ReadKey();
        }
        /// <summary>
        /// Метод с помощью которого вводится значение степени полинома и проверяется корректность введённых данных.
        /// </summary>
        /// <returns>Степень полинома.</returns>
        static uint InputPower()
        {
            uint value;

            while (!uint.TryParse(Console.ReadLine(), out value))
            {
                Console.WriteLine("Данные введнены некорректно, попробуйте ещё раз...");
                Console.Write("Степень: ");
            }

            return value;
        }
        /// <summary>
        /// Метод с помощью которого вводятся значения коэффициентов полинома и проверяется корректность введённых данных.
        /// </summary>
        /// <param name="count">Степень полинома.</param>
        /// <param name="odds">Пустой массив коэффициентов.</param>
        static void InputOdds(uint count, double[] odds)
        {
            for (uint i = 0; i <= count; i++)
            {
                Console.Write("Введите коэффициент аргумента " + i + " степени: ");
                while (!double.TryParse(Console.ReadLine(), out odds[i]))
                {
                    Console.WriteLine("Данные введнены некорректно, попробуйте ещё раз...");
                    Console.Write("Коэффициент: ");
                }
            }
        }
    }
}