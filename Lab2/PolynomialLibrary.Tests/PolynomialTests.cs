﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PolynomialLibrary.Tests
{
    [TestClass]
    public class PolynomialTests
    {
        [TestMethod]
        public void Polynomial_Test1()
        {
            uint power = 4;
            double[] odds = new double[] { 1, 2, 3, 4};

            Polynomial polynomial = new Polynomial(power, odds);

            Assert.AreEqual(power, polynomial.PolynomialPower);
            CollectionAssert.AreEqual(odds, polynomial.Odds);
        }

        [TestMethod]
        public void Polynomial_Test2()
        {
            uint power = 4;
            double[] odds = new double[] { -5, 99.1, 3.4356, -4.88 };

            Polynomial polynomial = new Polynomial(power, odds);

            Assert.AreEqual(power, polynomial.PolynomialPower);
            CollectionAssert.AreEqual(odds, polynomial.Odds);
        }

        [TestMethod]
        public void Polynomial_Test3()
        {
            uint power = 5;
            double[] odds = new double[] { 0, 0, 0.099999, -123, 0 };

            Polynomial polynomial = new Polynomial(power, odds);

            Assert.AreEqual(power, polynomial.PolynomialPower);
            CollectionAssert.AreEqual(odds, polynomial.Odds);
        }

        [TestMethod]
        public void Polynomial_Test4()
        {
            uint power = 6;
            double[] odds = new double[] { 45, -56.234, 473.6578, -123.567876, -1, 345745.000077 };

            Polynomial polynomial = new Polynomial(power, odds);

            Assert.AreEqual(power, polynomial.PolynomialPower);
            CollectionAssert.AreEqual(odds, polynomial.Odds);
        }

        [TestMethod]
        public void Polynomial_Test5()
        {
            uint power = 2;
            double[] odds = new double[] { -4326.1215, 4000.457578, -0.000000567567 };

            Polynomial polynomial = new Polynomial(power, odds);

            Assert.AreEqual(power, polynomial.PolynomialPower);
            CollectionAssert.AreEqual(odds, polynomial.Odds);
        }

        [TestMethod]
        public void PolynomialMultiplication_Test1()
        {
            uint firstPower = 2;
            double[] firstOdds = new double[] { -1, -1, -1 };
            uint secondPower = 2;
            double[] secondOdds = new double[] { 1, 1, 1 };
            uint expectedPower = 4;
            double[] expectedOdds = new double[] { -1, -2, -3, -2, -1 };

            Polynomial firsrPoly = new Polynomial(firstPower, firstOdds);
            Polynomial secondPoly = new Polynomial(secondPower, secondOdds);

            Polynomial thirdPoly = firsrPoly * secondPoly;

            Assert.AreEqual(expectedPower, thirdPoly.PolynomialPower);
            CollectionAssert.AreEqual(expectedOdds, thirdPoly.Odds);
        }

        [TestMethod]
        public void PolynomialMultiplication_Test2()
        {
            uint firstPower = 5;
            double[] firstOdds = new double[] { -6, 0, 0, 10, -5, 0 };
            uint secondPower = 8;
            double[] secondOdds = new double[] { -100, 7, 0, 0, 0, 0, 10, 0, 0 };
            uint expectedPower = 13;
            double[] expectedOdds = new double[] { 600, -42, 0, -1000, 570, -35, -60, 0, 0, 100, -50, 0, 0, 0 };

            Polynomial firsrPoly = new Polynomial(firstPower, firstOdds);
            Polynomial secondPoly = new Polynomial(secondPower, secondOdds);

            Polynomial thirdPoly = firsrPoly * secondPoly;

            Assert.AreEqual(expectedPower, thirdPoly.PolynomialPower);
            CollectionAssert.AreEqual(expectedOdds, thirdPoly.Odds);
        }

        [TestMethod]
        public void PolynomialMultiplication_Test3()
        {
            uint firstPower = 1;
            double[] firstOdds = new double[] { 0.5, 0.6 };
            uint secondPower = 2;
            double[] secondOdds = new double[] { 0.1, 0.56, 0 };
            uint expectedPower = 3;
            double[] expectedOdds = new double[] { 0.05, 0.34, 0.336, 0 };

            Polynomial firsrPoly = new Polynomial(firstPower, firstOdds);
            Polynomial secondPoly = new Polynomial(secondPower, secondOdds);

            Polynomial thirdPoly = firsrPoly * secondPoly;

            Assert.AreEqual(expectedPower, thirdPoly.PolynomialPower);
            CollectionAssert.AreEqual(expectedOdds, thirdPoly.Odds);
        }

        [TestMethod]
        public void PolynomialMultiplication_Test4()
        {
            uint firstPower = 3;
            double[] firstOdds = new double[] { 0.5, 0.6, -1, -0.45, -5.1 };
            uint secondPower = 2;
            double[] secondOdds = new double[] { 0.1, 0.6, 0 };
            uint expectedPower = 5;
            double[] expectedOdds = new double[] { 0.05, 0.36, 0.26, -0.645, -0.27, 0 };

            Polynomial firsrPoly = new Polynomial(firstPower, firstOdds);
            Polynomial secondPoly = new Polynomial(secondPower, secondOdds);

            Polynomial thirdPoly = firsrPoly * secondPoly;

            Assert.AreEqual(expectedPower, thirdPoly.PolynomialPower);
            CollectionAssert.AreEqual(expectedOdds, thirdPoly.Odds);
        }

        [TestMethod]
        public void PolynomialMultiplication_Test5()
        {
            uint firstPower = 4;
            double[] firstOdds = new double[] { -7.1, 5, -1.7, -8.5, 10, 56 };
            uint secondPower = 1;
            double[] secondOdds = new double[] { 0.1, 0.6 };
            uint expectedPower = 5;
            double[] expectedOdds = new double[] { -0.71, -3.76, 2.83, -1.87, -4.1, 6 };

            Polynomial firsrPoly = new Polynomial(firstPower, firstOdds);
            Polynomial secondPoly = new Polynomial(secondPower, secondOdds);

            Polynomial thirdPoly = firsrPoly * secondPoly;

            Assert.AreEqual(expectedPower, thirdPoly.PolynomialPower);
            CollectionAssert.AreEqual(expectedOdds, thirdPoly.Odds);
        }

        [TestMethod]
        public void MultiplyPolynomial_Test1()
        {
            uint power = 5;
            double[] odds = new double[] { -7.1, 5, -1.7, -8.5, 10, 56 };
            int multiplier = 0;
            uint expectedPower = 5;
            double[] expectedOdds = new double[] { 0, 0, 0, 0, 0, 0 };

            Polynomial polynomial = new Polynomial(power, odds);

            Polynomial outputPoly = multiplier * polynomial;

            Console.WriteLine("Expected: ");

            Assert.AreEqual(expectedPower, outputPoly.PolynomialPower);
            CollectionAssert.AreEqual(expectedOdds, outputPoly.Odds);
        }

        [TestMethod]
        public void MultiplyPolynomial_Test2()
        {
            uint power = 5;
            double[] odds = new double[] { -7.1, 5, -1.7, -8.5, 10, 56 };
            int multiplier = 10;
            uint expectedPower = 5;
            double[] expectedOdds = new double[] { -71, 50, -17, -85, 100, 560 };

            Polynomial polynomial = new Polynomial(power, odds);

            Polynomial outputPoly = multiplier * polynomial;

            Console.WriteLine("Expected: ");

            Assert.AreEqual(expectedPower, outputPoly.PolynomialPower);
            CollectionAssert.AreEqual(expectedOdds, outputPoly.Odds);
        }

        [TestMethod]
        public void MultiplyPolynomial_Test3()
        {
            uint power = 5;
            double[] odds = new double[] { -7.1, 5, -1.7, -8.5, 10, 56 };
            int multiplier = 4;
            uint expectedPower = 5;
            double[] expectedOdds = new double[] { -28.4, 20, -6.8, -34, 40, 224 };

            Polynomial polynomial = new Polynomial(power, odds);

            Polynomial outputPoly = multiplier * polynomial;

            Assert.AreEqual(expectedPower, outputPoly.PolynomialPower);
            CollectionAssert.AreEqual(expectedOdds, outputPoly.Odds);
        }

        [TestMethod]
        public void MultiplyPolynomial_Test4()
        {
            uint power = 5;
            double[] odds = new double[] { -7.1, 5, -1.7, -8.5, 10, 56 };
            int multiplier = 1;
            uint expectedPower = 5;
            double[] expectedOdds = new double[] { -7.1, 5, -1.7, -8.5, 10, 56 };

            Polynomial polynomial = new Polynomial(power, odds);

            Polynomial outputPoly = multiplier * polynomial;

            Assert.AreEqual(expectedPower, outputPoly.PolynomialPower);
            CollectionAssert.AreEqual(expectedOdds, outputPoly.Odds);
        }

        [TestMethod]
        public void MultiplyPolynomial_Test5()
        {
            uint power = 5;
            double[] odds = new double[] { -7.1, 5, -1.7, -8.5, 10, 56 };
            int multiplier = -1;
            uint expectedPower = 5;
            double[] expectedOdds = new double[] { 7.1, -5, 1.7, 8.5, -10, -56 };

            Polynomial polynomial = new Polynomial(power, odds);

            Polynomial outputPoly = multiplier * polynomial;

            Assert.AreEqual(expectedPower, outputPoly.PolynomialPower);
            CollectionAssert.AreEqual(expectedOdds, outputPoly.Odds);
        }
    }
}