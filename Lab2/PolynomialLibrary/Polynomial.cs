﻿using System;

namespace PolynomialLibrary
{
    public class Polynomial
    {
        /// <summary>
        /// Свойство хранящее степень полинома.
        /// </summary>
        public uint PolynomialPower { get; }
        /// <summary>
        /// Свойство хранящее коэффициенты полинома в массиве.
        /// </summary>
        public double[] Odds { get; }
        /// <summary>
        /// Конструктор. Создаёт полином.
        /// </summary>
        /// <param name="power">Степень полинома.</param>
        /// <param name="odds">Коэффициенты при аргументах.</param>
        public Polynomial(uint power, double[] odds)
        {
            PolynomialPower = power;
            Odds = new double[power];
            Odds = odds;
        }
        /// <summary>
        /// Перегрегрузка оператора '*'. Метод-обёртка.
        /// </summary>
        /// <param name="firstPoly">Первый полином.</param>
        /// <param name="secondPoly">Второй полином.</param>
        /// <returns>Полином полученный после умножения полиномов.</returns>
        public static Polynomial operator *(Polynomial firstPoly, Polynomial secondPoly)
        {
            return PolynomialMultiplication(firstPoly, secondPoly);
        }
        /// <summary>
        /// Перегрегрузка оператора '*'. Метод-обёртка.
        /// </summary>
        /// <param name="multiplier">Множитель.</param>
        /// <param name="polynomial">Полином.</param>
        /// <returns>Полином.</returns>
        public static Polynomial operator *(int multiplier, Polynomial polynomial)
        {
            return MultiplyPolynomial(multiplier, polynomial);
        }
        /// <summary>
        /// Переопределённый метод, который выводит полином в естественном виде.
        /// </summary>
        /// <returns>Полином в виде строки.</returns>
        public override string ToString()
        {
            uint i, decade = 0, one = 0;
            string value = null;
            bool isFirst = true;

            for (i = PolynomialPower; i >= 0 && i <= PolynomialPower && isFirst; i--)
            {
                if(i > 9)
                {
                    decade = i / 10;
                    one = i % 10;
                }

                switch (Odds[i])
                {
                    case 0:
                        break;
                    case 1:
                        isFirst = false;
                        break;
                    case -1:
                        value += "-";
                        isFirst = false;
                        break;
                    default:
                        value += Odds[i].ToString();
                        isFirst = false;
                        break;
                }

                switch (i)
                {
                    case 0:
                        if (Odds[i] == 1 || Odds[i] == -1)
                            value += "1";
                        break;
                    case 1:
                        if(Odds[i] != 0)
                        {
                            value += "x";
                        }
                        break;
                    default:
                        if (Odds[i] != 0)
                        {
                            if(i > 9)
                            {
                                value += "x" + DigitToSuperscript(decade) + DigitToSuperscript(one);
                            }
                            else
                                value += "x" + DigitToSuperscript(i);
                        }
                        break;
                }
            }

            for (; i >= 0 && i <= PolynomialPower && !isFirst; i--)
            {
                if (i > 9)
                {
                    decade = i / 10;
                    one = i % 10;
                }

                switch (Odds[i])
                {
                    case 0:
                        break;
                    case 1:
                        value += ConvertToSymbol(Odds[i]);
                        break;
                    case -1:
                        value += ConvertToSymbol(Odds[i]);
                        break;
                    default:
                        value += ConvertToSymbol(Odds[i]) + Math.Abs(Odds[i]).ToString();
                        break;
                }

                switch (i)
                {
                    case 0:
                        if (Odds[i] == 1 || Odds[i] == -1)
                            value += "1";
                        break;
                    case 1:
                        if (Odds[i] != 0)
                        {
                            value += "x";
                        }
                        break;
                    default:
                        if (Odds[i] != 0)
                        {
                            if (i > 9)
                            {
                                value += "x" + DigitToSuperscript(decade) + DigitToSuperscript(one);
                            }
                            else
                                value += "x" + DigitToSuperscript(i);
                        }
                        break;
                }     
            }

            if (value == null)
                value = " 0";

            return value;
        }
        /// <summary>
        /// Метод определяющий знак числа.
        /// </summary>
        /// <param name="value">Число, знак которого нужно определить.</param>
        /// <returns>Знак в виде символа.</returns>
        private string ConvertToSymbol(double value)
        {
            if (value < 0)
                return "-";
            else
                return "+";
        }
        /// <summary>
        /// Метод преобразующий число в код символа.
        /// </summary>
        /// <param name="digit">Число, которое преобразуется.</param>
        /// <returns>Символ юникода.</returns>
        private char DigitToSuperscript(uint digit)
        {
            switch (digit)
            {
                case 1: return '\u00b9';
                case 2: return '\u00b2';
                case 3: return '\u00b3';
                default: return (char)(0x2070 + digit);
            }
        }
        /// <summary>
        /// Перегрегрузка оператора '*'. Метод умножает два полинома.
        /// </summary>
        /// <param name="firstPoly">Первый полином.</param>
        /// <param name="secondPoly">Второй полином.</param>
        /// <returns>Полином полученный после умножения полиномов.</returns>
        private static Polynomial PolynomialMultiplication(Polynomial firstPoly, Polynomial secondPoly)
        {
            double[] newOdds = new double[firstPoly.PolynomialPower + secondPoly.PolynomialPower + 1];

            for (uint i = 0; i <= firstPoly.PolynomialPower; i++)
                for (uint j = 0; j <= secondPoly.PolynomialPower; j++)
                    newOdds[i + j] += firstPoly.Odds[i] * secondPoly.Odds[j];

            return new Polynomial(firstPoly.PolynomialPower + secondPoly.PolynomialPower, newOdds);
        }
        /// <summary>
        /// Перегрегрузка оператора '*'. Метод умножает полином на число.
        /// </summary>
        /// <param name="multiplier">Множитель.</param>
        /// <param name="polynomial">Полином.</param>
        /// <returns>Полином.</returns>
        private static Polynomial MultiplyPolynomial(int multiplier, Polynomial polynomial)
        {
            double[] newOdds = new double[polynomial.PolynomialPower + 1];

            for (uint i = 0; i <= polynomial.PolynomialPower; i++)
                newOdds[i] = polynomial.Odds[i] * multiplier;

            return new Polynomial(polynomial.PolynomialPower, newOdds);
        }
    }
}